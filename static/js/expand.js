let images = document.querySelectorAll("[data-action='expand']");

Array.from(images).forEach(function(element) {
    element.addEventListener('click', function(e) { expandMedia(this); e.preventDefault(); });
});

function expandMedia(el)
{
	let mediaType = el.getAttribute("data-type");

	if (el.tagName == "a")
		e.preventDefault()

	if ( mediaType == "image" )
	{
		let attr = el.getAttribute("data-image");
		el.classList.toggle('thumbnail');
		if(el.classList.contains('thumbnail'))
		{
			el.src = getThumbnailUri(attr);
		}
		else
		{
			el.src = encodeURI(attr);
		}
	}

	// This is unstable
	// Only reason this works is because browsers
	// skip the fact that player is inside div
	// when it comes to click listener
	if ( mediaType == "video" || mediaType == "audio" || mediaType == "embed" )
	{
		const id = el.getAttribute("data-id");
		const embed = document.getElementById('video_' + id);

		const http = new XMLHttpRequest();
		let url = 'https://nyymi.net/player.php?id=' + id

		if ( embed.classList.contains('expanded') )
		{
			url += "&close";
		}

		http.open( "GET", url );
		http.send();
		http.onload = () => {
			embed.innerHTML = http.responseText
			if ( embed.classList.contains('expanded') )
			{
				embed.style.width = '200px';
				embed.classList.remove('expanded');
			}
			else
			{
				embed.style.width = '500px';
				embed.classList.add('expanded');
			}
		}
	}
}

function expandAllMedia() {
    Array.from(images).forEach(function(element) {
		expandMedia(element);
    });
}

function getThumbnailUri(imgUri) {
    return  "https://i.nyymi.net/" + imgUri.split("/")[3] + "/t/thumb.avif";
}
