let referenceLinks = document.querySelectorAll('a.messageReference')

referenceLinks.forEach(
	function(link) {
		link.addEventListener( 'mouseover', mouseOverHandler );
	}
);

async function mouseOverHandler(e)
{
	if (e.target.dataset.action != "hoverQuote")
	{
		return
	}
	
	let target = e.target;
	let dataset = target.dataset
	let action = dataset.action

	linkLocation = e.target.getBoundingClientRect()
	
	const post = new FormData();
	post.append("query", "quoted");
	post.append("id", dataset.id);
	post.append("apikey", getCookie("apikey"));

	await fetch("https://api.nyymi.net/message.php", {
		method: 'POST',
		credentials: 'include',
		body: post
	})
	.then( async response => {
		
		const floatingQuote = document.createElement("div");
		floatingQuote.id = "floatingQuote_" + dataset.id
		floatingQuote.style.left = linkLocation.left + "px";
		floatingQuote.style.top = (linkLocation.top + 15) + "px";
		floatingQuote.classList.add("answer");
		floatingQuote.classList.add("floatingQuote");

		switch (response.status) {
			case 200:
				parsed = await response.json();
				fUsername = parsed.username
				fTime = parsed.time
				fMessage = parsed.message
				fFiledir = parsed.filedir
				break
			case 401:
				fUsername = "Mister 401"
				fTime = "3000-02-31T00:00:00Z" // User has no rights in the distant future
				fMessage = "Insufficient rights"
				fFiledir = null
				break
			case 404:
				fUsername = "Mister 404"
				fTime = "4040-04-04T04:04:40Z"
				fMessage = "Message not found"
				fFiledir = null
				break
			case 500:
				fUsername = "Mister 401"
				fTime = "5000-05-05T05:00:50Z"
				fMessage = "Api fucked up, contact admins in /meta/"
				fFiledir = null
				break
			default:
				fUsername = "Mister ???"
				fTime = "1970-01-01T00:00:00Z"
				fMessage = "Error that isn't handled"
				fFiledir = null
				break
		}
		console.log(parsed)
		console.log(response.status)
		
		const header = document.createElement("div");
		header.classList.add("poster_info");
		
		const username = document.createElement("span");
		username.classList.add("username");
		username.innerHTML = fUsername
		header.appendChild(username);
		
		let timetag = document.createElement("time");
		timetag.innerHTML = unixToHuman(fTime, "de-DE");
		header.appendChild(timetag);
		
		floatingQuote.appendChild(header);

		if(fFiledir) {
			let thumb = document.createElement("img");
			thumb.style.float = "left"
			thumb.src = "https://i.nyymi.net/" + fFiledir + "/t/thumb.avif"
			floatingQuote.appendChild(thumb);
		}
		
		let messagediv = document.createElement("div");
		messagediv.classList.add("message");
		messagediv.innerHTML = fMessage;
		
		floatingQuote.appendChild(messagediv);
		document.body.append(floatingQuote);
	
		removeFlaotingReference(floatingQuote, e.target);

	})
}

function removeFlaotingReference(floating, triggerLink)
{
	setTimeout(function() {

		if( floating.matches(':hover') || triggerLink.matches(':hover') ) {
			removeFlaotingReference(floating, triggerLink)
		} else {
			floating.remove()
		}
	}, 1000);
}

async function mouseOutHandler(e)
{
	if (e.target.dataset.action != "hoverQuote")
	{
		return
	}
	
	let target = e.target;
	let dataset = target.dataset
	let action = dataset.action
	
	document.getElementById("floatingQuote_" + dataset.id).remove()
	
}
