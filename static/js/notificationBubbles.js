function newNotification(title, description, type="unknown") {
	const allowed_notification_types = ["info", "success", "warning", "error", "unknown"];
	
	if(title == "") {
		console.log("Push notification was called without title, consider fixing.\n");
	}
		
	if(description == "") {
		console.log("Push notification was called without content.\n");
		return;
	}
		
	if (!allowed_notification_types.includes(type)) {
		console.log("Not allowed type for push_notification() was used.\n");
		return;
	}
	
	const timenow = Date.now()
    const notificationMainDiv = document.querySelector("#notifications")
    const newNotification = document.createElement("div");
    newNotification.setAttribute("type", "button")
    newNotification.classList.add("notification")
    newNotification.classList.add(type)
    newNotification.setAttribute("data-action", "hide-notification")
    newNotification.setAttribute("data-time", timenow)
    newNotification.setAttribute("onclick", 'this.remove()')
    newNotification.innerHTML = "<b>" + title + "</b><br />" + description


    notificationMainDiv.appendChild(newNotification)
	
    setTimeout(() => {
        newNotification.classList.add("fadeOut")
        setTimeout(() => {
            newNotification.remove()
        }, 2000)
    }, 5000)
}
