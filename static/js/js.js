let quotevisible = 0;
let mouseX = 0;
let mouseY = 0;
//document.cookie

function getCookie(cname) {
	const cookiearr = decodeURIComponent(document.cookie).split(';')
	let splitted = null
	let apikey = null
	cookiearr.forEach((elem) => {
		splitted = elem.split('=')
		if(splitted[0] == " " + cname) {
			apikey = splitted[1]
			return
		}
	});
	return apikey
}

function removeElement(elementId) {
    const element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
}

document.addEventListener('click', async (e) => { await clickHandler(e) });

async function clickHandler(e) {
	if (!e.target.dataset.action)
		return
	
    let target = e.target;
    let dataset = target.dataset
	let action = dataset.action
	
	//target.classList
	//ctarget.tagName

	if (action == "expand") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'expandVideo' was called but data-id is missing")
		if (!dataset.type)
			return newNotification("Missing dataset", "Data-action 'expandVideo' was called but data-type is missing")

		if (dataset.type == "video")
		{
		   	videoexpander(dataset.id)
		}
	}
	
	if (action == "closeVideo") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'closeVideo' was called but data-id is missing")

		hidevideo(dataset.id)
		
	}
	
	if (action == "toggleVisibility") {
		let targetElement = document.getElementById(dataset.id)
		 targetElement.style.display = targetElement.style.display == "none" ? "block" : "none"
	}
	if (action == "remove") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'remove' was called but data-id is missing")

		let targetElement = document.getElementById(dataset.id)
		
		targetElement.style.display = targetElement.style.display == "none" ? "block" : "none"
	}
	
	if (action == "undim") {
		removeElement("dimmer");
	}
	
    if (action == "unblur") {
		e.stopPropagation();
		e.preventDefault();
		if(!target.classList.contains("blur"))
			return newNotification("Missing class", "Data-action unblur was called but self didn't have blurred class", "error")

		target.classList.remove("blur");
		delete dataset.action;
		if(dataset.url)
			target.outerHTML = "<a href='"+dataset.url+"'>"+target.outerHTML+"</a>"
	}
	
	/* Message button actions */
	// missing follow
	if (action == "deleteMessage") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'deleteMessage' was called but data-id is missing")

    	APIaction('https://api.nyymi.net/action.php?action=delete', dataset.id);
    	document.getElementById("message_" + dataset.id).innerHTML = "";
	}
	
	if (action == "report") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'report' was called but data-id is missing")

		Dim();
	
		const dimDiv = document.createElement("div");
		dimDiv.classList.add('dimbox')
		document.getElementById("dimmer").appendChild(dimDiv);
	
		dimDiv.innerHTML += '<h3>Report</h3>';
		dimDiv.innerHTML += '<select id="selections"></select><br /><br />';
		dimDiv.innerHTML += '<br><button class="left" data-action="undim">Close</button>';
		dimDiv.innerHTML += '<button data-action="reportSend" data-id="'+dataset.id+'"  class="right">Report</button>';
	
		const http = new XMLHttpRequest();
		const url='https://api.nyymi.net/read.php?query=banreasons&id='+dataset.id;

		const formData = new FormData();
		formData.append('apikey', getCookie("apikey"));
		
		http.open( "POST", url );
		http.send(formData);
		http.onload = () => {
			var obj = JSON.parse(http.responseText);
			
		};
	}
	
	if (action == "openLoginForm")
	{
		Dim();
		
		const dimDiv = document.createElement("div");
		dimDiv.classList.add('dimbox')
		dimDiv.classList.add('loginbox')
		document.getElementById("dimmer").appendChild(dimDiv);

		let usernameInput = document.createElement("input");
		usernameInput.type = "text"
		usernameInput.placeholder = "username"
		usernameInput.name = "username"
		usernameInput.setAttribute("id", "loginname");
		
		let passwordInput = document.createElement("input");
		passwordInput.type = "password"
		passwordInput.placeholder = "password"
		passwordInput.name = "password"
		passwordInput.setAttribute("id", "password");
		
		let submitButton = document.createElement("input");
		submitButton.type = "submit"
		submitButton.value = "Login"
		submitButton.dataset.action = "login"
		
		dimDiv.appendChild(usernameInput);
		dimDiv.appendChild(passwordInput);
		dimDiv.appendChild(submitButton);
		
	}
	
	if (action == "login")
	{
		const loginname = document.getElementById("loginname").value
		const password  = document.getElementById("password").value
		
		const post = new FormData();
		post.append('username', loginname);
		post.append('password', password);
		post.append('submit', '1');

		await fetch("https://api.nyymi.net/login.php", {
			method: 'POST',
			credentials: 'include',
			body: post
		})
		.then( async response => {
		
			parsed = await response.json();
			
			if ( parsed.error )
			{
				newNotification(parsed.errorTitle, parsed.errorDesc, parsed.errorType)
				return
			}
			
			
			if ( parsed.success )
			{
				newNotification("Logged in", "You have succesfully logged in.", "success")
				location.reload();
			}
			
			return
			
		})
		
	}
	
	if (action == "logout") {
		let confir = confirm("Are you sure?")
		if(confir)
		{
			APIaction('https://nyymi.net/logout.php');
			location.reload();
		}
	}
	
	if (action == "reportSend") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'reportMessage' was called but data-id is missing")

    	APIaction('https://api.nyymi.net/action.php?action=report&id='+dataset.id);
	}
	
	if (action == "unreportMessage") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'unreportMessage' was called but data-id is missing")
	    APIaction('https://api.nyymi.net/action.php?action=report&check=1&id='+dataset.id);
	}
	
	if (action == "lockThread") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'lockThread' was called but data-id is missing")
		APIaction('https://api.nyymi.net/action.php?action=lock&id='+dataset.id);
	}

	if (action == "stickyThread") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'stickyThread' was called but data-id is missing")
		APIaction('https://api.nyymi.net/action.php?action=sticky&id='+dataset.id);
	}
	
	if (action == "hideMessage") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-action 'hideMessage' was called but data-id is missing")
		APIaction('https://api.nyymi.net/action.php?action=hide&id='+dataset.id);
		
		messageDom = document.getElementById('message_' + dataset.id)

		var unhideButton = document.createElement("a");
		unhideButton.classList.add('icon-plus')
		unhideButton.dataset.action = "unhide"
		unhideButton.style.margin = "5px"
		unhideButton.dataset.id = dataset.id

		messageDom.replaceChildren(unhideButton)

		unhideButton.addEventListener('click', async (e) => { await clickHandler(e) });

	}
	
	if (action == "quoteMessage") {
		if (!dataset.id)
			return newNotification("Missing dataset", "Data-quoteMessage 'hideMessage' was called but data-id is missing")
		
		add(">>" + dataset.id);
		
	}
	
	// Admin stuff
	
	if (action == "changeUserLevels")
	{
		APIaction('https://api.nyymi.net/action.php?action=userLevels&id='+dataset.id);
	}
}

function UnHideMessage(id) {
	// "#message_" + id).load('/scripts/hide.php?id=' + id);
}

function hideshow(which){
	if (!document.getElementById)
		return
	if (which.style.visibility=="hidden")
		which.style.visibility="visible"
	else
		which.style.visibility="hidden"
}

function hideelement(id) {
	console.log(":DDDDD");
	document.getElementById(id).style.visibility = "hidden";
}
	
function LoadPost(id) {
	"#thread_" + id.load('/load_post.php?id=' + id);
}
	
function add(text) {
	document.getElementById("message_box").value += text + "\n";
}

function APIaction(url, id=null) {
	const r = confirm('Are you sure?');
	if (r == true) {
		const http = new XMLHttpRequest();
		http.withCredentials = true;
		http.open( "POST", url );
		const formData = new FormData();
		
		if(id)
		{
			formData.append('id', id);
		}
		
		http.send(formData);
		http.onload = () => {
			response = http.responseText
			console.log( url );
			console.log( response );
			return response
		}
    }
}

// document.querySelector(".thread >div:last-child")

async function getNewMessages() {
	return
	let thread = document.querySelector(".thread")
	let lastMessage = document.querySelector(".thread >div:last-child")
	let id = thread.dataset.id
	let lastId = lastMessage.dataset.id

	const formData = new FormData();
	formData.append('lastid', lastId);
	formData.append('id', id);
	
	await fetch("https://nyymi.net/get_new_messages.php", {
		method: 'POST',
		credentials: 'include',
		body: formData
	})
	.then(response => response.text())
	.then(result => {
		lastId++
		if(result !== "") {
			let templateElement = document.createElement("div");
			templateElement.classList.add('answer')
			templateElement.dataset.id = lastId
			templateElement.id = "message_" + lastId
			templateElement.innerHTML = result; 
			thread.appendChild(templateElement);
			
			templateElement.addEventListener('click', function(e) { expandMedia(this); e.preventDefault(); });
			templateElement.addEventListener('click', async (e) => { await clickHandler(e) });
			
		}
	})
	.catch(error => {
		console.log(error)
	});
}

if(document.querySelector(".thread"))
{
	setInterval(getNewMessages, 10000);
}

//Here is code for action box (ex. when reporting message)
function Dim() {
	var ActionBox = document.createElement("div");
	ActionBox.classList.add('dimmer')
	ActionBox.setAttribute("id", "dimmer");
	ActionBox.dataset.action = "undim"
	document.body.appendChild(ActionBox);
}

function unixToHuman(unixtime, zone)
{
	const options = {
		year: 'numeric',
		month: 'numeric',
		day: 'numeric',
		hour: 'numeric',
		minute: 'numeric',
		second: 'numeric'
	};
	
	const date = new Date(unixtime);
	return date.toLocaleDateString('de-DE', options)
	
}

const times = document.getElementsByTagName("time");

for (const time of times)
{
	time.innerHTML = unixToHuman(time.innerHTML, "de-DE")
}

window.addEventListener("load", (event) => {
	hljs.initHighlightingOnLoad();
});


//Under this is warning message to console
let devtools = function(){};
devtools.toString = function() {
    this.opened = true;
    const warningTitleCSS = 'color:red; font-size:60px; font-weight: bold; -webkit-text-stroke: 1px black;';
	const warningDescCSS = 'font-size: 18px;';
	console.log('%cHello dear anon!', warningTitleCSS);
	console.log("%cIf you are stupid enough to post something here by recommendation from another user, you are fucking dumbass and deserve all shit coming to you.", warningDescCSS);
}
