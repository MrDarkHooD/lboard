var fileReader = new FileReader();
fileReader.onloadend = function(e) {
	var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
	var header = "";
	for(var i = 0; i < arr.length; i++) {
		header += arr[i].toString(16);
	}
	console.log(header);

	// Check the file signature against known types
};

	
const uploadFiles = (url, files, onProgress) =>
	new Promise((resolve, reject) => {
		document.getElementById('upload-progress').style="display:block;"
		const xhr = new XMLHttpRequest();
		xhr.upload.addEventListener('progress', e => onProgress(e.loaded / e.total));
		xhr.addEventListener('load', () => resolve({ status: xhr.status, body: xhr.responseText }));
		xhr.addEventListener('error', () => reject(new Error('File upload failed')));
		xhr.addEventListener('abort', () => reject(new Error('File upload aborted')));
		xhr.open('POST', url, true);
		xhr.withCredentials = true;
		const formData = new FormData();
		formData.append("file", files[0]);
		formData.append("apikey", getCookie("apikey"));
		xhr.send(formData);
	}
);

window.addEventListener('paste', e => {
	
	var obj = document.getElementById("message");
	if (document.activeElement === obj) {
		fileInput = document.getElementById("fileInput");
		fileInput.files = e.clipboardData.files;
		
		var element = document.getElementById('file');
		var event = new Event('change');
		element.dispatchEvent(event);
	}
});

function clearFileAndDirInput() {
	document.getElementById('filedir').value = ""
	document.getElementById('fileInput').value = ""
	//document.getElementById('fileprewiev').innerHTML = ""
}

const chooseFileHandler = (e) => {
  e.preventDefault();
  document.getElementById('fileInput').click();
};

document.querySelector("[data-action='choose-file']").addEventListener('touchstart', chooseFileHandler);
document.querySelector("[data-action='choose-file']").addEventListener('click', chooseFileHandler);




document.getElementById('fileInput').addEventListener('change', async e => {
	const fileInInput = e.currentTarget.files[0]
	const fileInInputImagetype = fileInInput.type.split(/\//)[0]
	const fileInInputExtension = fileInInput.name.split(/\./).pop()
	
	// Read rules and check user input to help legit users
	const http = new XMLHttpRequest();
	const url='https://api.nyymi.net/fileRestrictions.php?query=all'; 
	http.withCredentials = true;
	http.open( "POST", url );
	
	http.send();

	http.onload = () => {
		var rules = JSON.parse(http.responseText);
		const fileSize = fileInInput.size
		if (fileSize > rules.max_filesize) {
			clearFileAndDirInput()
			newNotification("File too large", "According to your browser your file is " + Math.round(fileSize / 1024 / 1024) + "MiB and maxium allowed is "+rules.max_filesize*1024*1024+"MiB", "error")
			return
		}
		if (!rules.allowed_mimetypes.includes(fileInInput.type)) {
			clearFileAndDirInput()
			newNotification("Mimetype not allowed", "According to your browser your file's mimetype is " + fileInInput.type, "error")
			return
		}
	
		if (!rules.allowed_extensions.includes(fileInInputExtension)) {
			console.log(fileInInput)
			clearFileAndDirInput()
			newNotification("Extension not allowed", "According to your browser your file's extension is " + fileInInputExtension, "error")
			return
		}
		
/*		rules.extension_to_filetype.forEach((ext, type) => {
			if (fileInInputExtension == ext) {
				if(type != fileInInputImagetype) { //yes i know...
					clearFileAndDirInput()
					newNotification("Extension not suitable for filetype", "According to your browser your file's extension is " + fileInInputExtension + " and imagetype is " + fileInInputImagetype, "error")
					return
				}
			}
		}); */
		
		// Still missing filenamelimiter and checker for naughty chars
	
	}
	console.log("still going")
	if(document.getElementById('fileInput').files.length == 0) 
		return
	console.log('still?')
	
	// All good says js, lets proceed
	const onProgress = progress => document.getElementById('upload-progress').value=Math.round(progress * 100);
	const response = await uploadFiles('https://api.nyymi.net/uploadFile.php', e.currentTarget.files, onProgress);
	if (response.status >= 400) {
		alert(`File upload failed - Status code: ${response.status}`);
		document.getElementById('upload-progress').style="display:none;"
	}
	const body = response.body
	console.log('Response:', body);
	document.getElementById('upload-progress').style="display:none;"
	
	const parsedBody = JSON.parse(body)
	document.getElementById("filedir").value = parsedBody.filedir;
	document.getElementById("file_preview").innerHTML = "<img src='"+parsedBody.thumbnail+"' style='width:100%;'>";
	document.getElementById('fileInput').value = ""
	if (response.status == 200) {
		var element = document.getElementById('filedir');
		var event = new Event('change');
		element.dispatchEvent(event);
	}
});
	
function fileInputCleared() {
	return new Promise(function (resolve, reject) {
		
			document.getElementById('filedir').addEventListener('change', function () {
				if(document.getElementById('fileInput').files.length > 0) {
					resolve();
				}
			});
	});
}

const sendMessage = () =>
	new Promise(async (resolve, reject) => {
		
		if(document.getElementById('fileInput').files.length > 0)
		{
			console.log("Waiting for file to upload")
			await fileInputCleared();
			console.log("File has been uploaded, proceed..")
		}
		
		const xhr = new XMLHttpRequest();
		xhr.upload.addEventListener('progress', e => console.log(e.loaded / e.total));
		xhr.addEventListener('load', () => {
			const responseJson = JSON.parse(xhr.responseText)
			
			if(responseJson.success) {
				newNotification("Message sent", "Your message was succesfully sent.", "success")
				document.querySelector('.postform').reset()
				document.querySelector('#file_preview').innerHTML = ""
				if(responseJson["forward"]) {
					url = "https://nyymi.net" + responseJson["forward"]
					console.log(url)
					console.log(responseJson["forward"])
					window.location.href = responseJson["forward"];
				}else {
					getNewMessages()
				}
			}else {
				newNotification(responseJson.errorTitle, responseJson.errorDesc, responseJson.errorType)
				return
			}
			
			resolve({ status: xhr.status, body: xhr.responseText })
		});
		xhr.addEventListener('error', () => reject(new Error('Message post failed')));
		xhr.addEventListener('abort', () => reject(new Error('Message post aborted')));
		xhr.open('POST', 'https://api.nyymi.net/postMessage.php', true);
		xhr.withCredentials = true;
		const formData = new FormData();
		document.querySelectorAll('#messagePostForm input').forEach((obj) => {
			if (obj.name != 'file')
			{
				formData.append(obj.name, obj.value);
			}
		});
		formData.append('apikey', getCookie("apikey"));
		formData.append('message_box', document.querySelector('textarea').value);
		console.log(formData)
		xhr.send(formData);
	}
);
	
document.querySelector(".postform").addEventListener("submit", async (event) => {
	event.preventDefault();
	sendMessage()
//	return
});

document.querySelector("#postbutton").addEventListener('touchstart', (event) => {
	event.preventDefault();
	sendMessage()
//	return
});

document.body.addEventListener('keydown', (event) => {
	var obj = document.getElementById("message_box");
	if (document.activeElement === obj) {
    	if(event.key === "Enter" && (event.metaKey || event.ctrlKey)) {
			event.preventDefault();
			sendMessage()
			return
    	}
	}
});
