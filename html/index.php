<?php
require_once("inc/topinclude.php");

$html = new html\HTML();
$html->printPageTop($site->name);

switch( $_GET["page"] ?? null )
{
	case "faq":
		require_once("inc/faq.html");
		break;

	case "cookies":
		require_once("inc/cookies.html");
		break;

	case "gdpr":
		echo "Tämä on EU:n yleisen tietosuoja-asetuksen (GDPR) mukainen rekisteri- ja tietosuojaseloste.<br />";
		echo "Laadittu 24.05.2018. Viimeisin muutos " . date("Y-m-d H:i:s.", filemtime("/www/src/GDPR.txt")) . "<br /><br />";
		echo file_get_contents("/www/src/GDPR.txt");
		break;

	case "il": // Interesting links
		require_once("inc/il.html");
		break;

	case "rules":
		echo  "<h3>Rules</h3>\n";

		$query = $DB->query(
			$sql->frontPageRules,
			fetchmode: PDO::FETCH_OBJ
		);

		$current_parent_id = null;
		echo  "<dl>\n";
		foreach ( $query as $row )
		{
			if ($current_parent_id !== $row->parent_id)
			{
				echo "\t<dt><b>$row->parent_reason</b></dt>\n";
				$current_parent_id = $row->parent_id;
			}

			if ($row->child_reason !== null)
				echo "\t<dd>$row->child_reason</dd>\n";
		}
		
		echo "</dl>\n";
		echo  "<br />\n";
		echo "Ilmoitamme kaikista ilmoitusvelvollisuuden alaisista lakirikkeistä viranomaisille.<br />\n";
		break;

	case "credits":
		require_once("inc/credit.html");
		break;

	default:

		$messageCount = $DB->single($sql->messageCount);
		$online = $DB->single($sql->usersOnline); //Last 5 minutes
		$average = $DB->single($sql->userCountPast24h);

		echo "<div style='border: 2px solid gray;width:100%;'>\n";
		echo "\t<div style='display:flex;flex-direction:row;'>\n";
		echo "\t\t<div>\n";
		echo "\t\t\t<img src='$path->logo' width='230px'>\n";
		echo "\t\t</div>\n";
		echo "\t\t<div>\n";
		echo "\t\t\t<center><h1>$site->name</h1></center>\n";
		echo "\t\t\t<center><h2>$site->slogan</h2></center>\n";

		echo "\t\t\t<center>[" . lang->FrontpageMessageCount . ": $messageCount | ";
		echo lang->FrontpageOnlineUsers . ": $online ]</center>\n";

		echo "\t\t\t<center>Avarage of $average daily users!</center>";

		echo "\t\t\t<center><div class='navmenu'>\n";
		echo "[ <a href='/?page=faq'>" . lang->FrontpageNavFAQ . "</a> | ";
		echo "<a href='/?page=rules'>" . lang->FrontpageNavRules . "</a> | ";
		echo "<a href='/?page=credits'>" . lang->FrontpageCredits . "</a> | ";
		echo "<a href='/?page=il'>" . lang->InterestingLinks . "</a> ]\n";
		echo "\t\t\t</div></center>\n";

		echo "\t\t</div>\n";
		echo "\t</div>\n";
		echo "</div>\n";

		echo "<table style='border:0px;width:100%;'>\n";
		echo "\t<tr align='center'>\n";
		echo "\t\t<td style='vertical-align:top;border:2pxsolid gray;width:70%;' rowspan=2>\n";
		echo "\t\t\t<center><h3>" . lang->FrontpageRecentImages . "</h3></center>\n";
		echo "\t\t\t<div class='frontpage-thumbs' style='display:flex;flex-wrap:wrap;'>\n";

		$images = $DB->query(
			$sql->frontPageRecentImages,
			fetchmode: PDO::FETCH_OBJ
		); 

		if( $images )
		{
			foreach ( $images as $image )
			{
				$html->printThumbnail(
					dir: $image->filedir,
					action: 'redirect',
					messageid: $image->messageid,
					blurrable: true,
					nsfw: $image->nsfw,
				);
			}
		}
		else
		{
			echo lang->ErrorFailedToLoadRecentMedia . "\n";
		}

		echo "\t\t\t</div>\n";
		echo "\t\t</td>\n";
		echo "\t\t<td style='width:50%;vertical-align: top;border: 2px solid grey;'>\n";
		echo "\t\t\t<center><h3>" . lang->FrontpageBoardList . "</h3></center>\n";
		echo  $html->htmlBoardList();
		echo "\t\t</td>\n";
		echo "\t</tr>\n";
		echo "\t<tr>\n";
		echo "\t\t<td style='width:50%;vertical-align: top;border: 2px solid grey;'>\n";
		echo "\t\t\t<center><h3>" . lang->FrontpageRecentThreads . "</h3></center>\n";
		echo "<ul>\n";

		$newest = $DB->query(
			$sql->frontPageRecentThreads,
			fetchmode: PDO::FETCH_OBJ
		);

		foreach( $newest as $new )
		{
			echo  "\t\t\t\t\t\t\t\t\t<li>";
			echo "<a href='./scripts/redirect.php?id=$new->id'>$new->topic</a>";
			echo "/$new->subboard/</li>\n";
		}

		echo "\t\t\t</ul>\n";
		echo "\t\t</td>\n";
		echo "\t</tr>\n";
		echo "</table>\n";
		break;
}

$html->printPageBottom();
$DB->closeConnection();
?>
