<?php
require_once("inc/topinclude.php");

$board = new stdClass();
$thread = new stdClass();

$urlPostfix = $_GET["b"] . "/"; // Add trailing slash for explode to prevent error
$urlPostfix = preg_replace("#/{2,}#", "/", $urlPostfix); // Remove extra slashes
list( $board->dir, $thread->id ) = explode( "/", $urlPostfix );

if( isset( $boards->redirect[$board->dir] ) )
{
	header( "Location: /" . $boards->redirect[$board->dir] . "/" . $thread->id );
	exit;
}

$html = new html\HTML();
$html->grid = (!$thread->id && $user->grid);

$board->limitResults = ($html->grid) ? 100 : 10;

$specialAreas = [
	"overboard" => [
		"name" 	=> "Overboard",
		"des" 	=> "All threads",
		"level" => 1,
		"nsfw" 	=> 0,
	],
	"own" => [
		"name" 	=> "Own threads",
		"des" 	=> "",
		"level" => 1,
		"nsfw" 	=> 0,
	],
	"replied" => [
		"name" 	=> "Replied threads",
		"des" 	=> "",
		"level" => 1,
		"nsfw" 	=> 0,
	],
	"hidden" => [
		"name" 	=> "Hidden threads",
		"des" 	=> "",
		"level" => 1,
		"nsfw" 	=> 0,
	],
	"followed" => [
		"name" 	=> "Followed threads",
		"des" 	=> "",
		"level" => 1,
		"nsfw" 	=> 0,
	]
];

if ( in_array( $board->dir, array_keys( $specialAreas ) ) )
{
	$area = $specialAreas[$board->dir];
}
else
{
	$area = $DB->row("
SELECT
	name,
	des,
	bitwise_level as level,
	nsfw
FROM
	area
WHERE
	dir = ?",
		array( $board->dir )
	);
}

if(!$area)
{
	$html->printPageTop();
	echo $lang->print('ErrorSubBoardNotFound');
	$html->printPageBottom();
	exit;
}

$board->name 	= $area["name"];
$board->url 	= "/" . $board->dir . "/";
$board->des 	= $area["des"];
$board->level 	= $area["level"];
$board->nsfw 	= $area["nsfw"];

if($thread->id)
{
	$board->listtype = 'thread';
}
else
{
	$board->listtype = ($user->grid) ? 'grid' : 'list';
}

if( !$access->hasLevel(UserAccessLevel, $board->level) )
{
	$html->printPageTop();
	echo $lang->print('ErrorNoRights');
	$html->printPageBottom();
	exit;
}

$sql = "
	 SELECT 
	 	bt.id,
		bt.filedir,
		bt.uid,
		bt.message,
		bt.embed_id,
		bt.username,
		bt.moderator,
		bt.admin,
		bt.floatright,
		bt.sage,
		bt.country,
		bt.mobile,
		bt.tor,
		bt.hideop,
		to_char(to_timestamp(unixtime)::timestamp, 'YYYY-MM-DD\"T\"HH24:MI:SS\"Z\"') as isotime,
		bt.locked,
		bt.sticky,
		bt.threadid,
		bt.tuid,
		bt.file_deleted,
		bt.banned_for_this,
		bt.banned_reason,
		bt.topic,
		bt.startpost,
		ba.dir,
		COALESCE(file.imagetype, NULL) AS filetype,
		COALESCE(file.deleted, NULL) AS og_file_deleted,
		COALESCE(file.filename, NULL) AS filename,
		COALESCE(file.extension, NULL) AS fileextension,
		COALESCE(file.size, NULL) AS filesize,
		COALESCE(file.mimetype, NULL) AS mimetype,
		COALESCE(file.lenght, NULL) AS filelenght,
		COALESCE(file.virus, NULL) AS filevirus,
		(
			SELECT COUNT(id)
			FROM board_messages AS btc
			WHERE
				btc.threadid = bt.id
				AND btc.startpost = false
				AND btc.deleted = false
		) AS reply_count,
		(
			SELECT COUNT(id)
			FROM hidden AS hid
			WHERE
				hid.messageid = bt.id
		) AS hidden_count,
		(
			SELECT 1
			FROM hidden AS hid
			WHERE
				hid.messageid = bt.id
				AND userid = :uid
		) AS hided,
		(
			SELECT 1
			FROM followed AS fol
			WHERE
				fol.threadid = bt.threadid
				AND userid = :uid
		) AS followed,
		(
			SELECT COUNT(id)
			FROM followed AS fol
			WHERE
				fol.threadid = bt.threadid
		) AS follow_count,
		(
			SELECT uid
			FROM board_messages AS sp
			WHERE
				sp.id = bt.threadid
		) AS puid,
		(
			SELECT 1
			FROM reports AS rep
			WHERE
				rep.messageid = bt.id
				AND dismissed = false
		) AS reported,
		(
			SELECT ARRAY(
				SELECT messageid 
				FROM
					replies as rep -- .columns 
				WHERE 
					rep.replies = bt.id
			)
		) AS replies,
		(
			SELECT 1
			FROM
				board_messages as sp 
			WHERE 
				sp.id = bt.threadid
		) AS startpost_deleted
	FROM 
		board_messages bt
	JOIN 
		area ba ON bt.area = ba.id
	LEFT JOIN 
		file ON bt.filedir = file.filedir
	WHERE
		bt.deleted = false";

if ( in_array( $board->dir, array_keys( $specialAreas ) ) )
{

	if ($board->dir == "own")
	{
		$sql .= "
		AND bt.startpost = true
		AND bt.uid = :uid
	ORDER BY
		bt.bump DESC
	LIMIT :limit";

		$params = array(
			"uid" => $user->uid,
			"limit" => $board->limitResults
		);
	}
		
	if ($board->dir == "hidden")
	{
		$sql .= "
		AND bt.startpost = true
		AND EXISTS
		(
			SELECT id
			FROM hidden
			WHERE
				messageid = bt.id
				AND uid = :uid
		)
	ORDER BY
		bt.bump DESC
	LIMIT :limit";
			
		$params = array(
			"uid" => $user->uid,
			"limit" => $board->limitResults
		);
	}

	if ($board->dir == "followed")
	{
		$html->printPageTop();
		echo $lang->print('ErrorPageDisabled');
		$html->printPageBottom();
		exit;
	}

	if ($board->dir == "replied")
	{
		$sql .= "
		AND bt.startpost = true
		AND EXISTS
		(
			SELECT id
			FROM board_messages
			WHERE
				uid = :uid
				AND threadid = bt.threadid
				AND deleted = 'false'
		)
		AND NOT EXISTS
		(
			SELECT id
			FROM board_messages
			WHERE
				uid = :uid
				AND id = bt.threadid
				AND deleted = 'false'
		)
	ORDER BY
		bt.bump DESC
	LIMIT :limit";
		
		$params = array(
			"uid" => $user->uid,
			"limit" => $board->limitResults
		);

	}
		
	if ($board->dir == "overboard")
	{
		$sql .= "
		AND bt.startpost = true
		AND ba.bitwise_level  = 1
	ORDER BY bt.bump DESC
	LIMIT :limit";

		$params = array(
			"uid" => $user->uid,
			"limit" => $board->limitResults
		);
	}
}
else
{
	if ( $thread->id )
	{
		$sql .=	"
		AND bt.threadid = :threadid
		AND NOT EXISTS
		(
			SELECT 1
			FROM board_messages
			WHERE
				id = bt.threadid
				AND deleted = 'true'
		)
	ORDER BY
		bt.id";

		$params = array(
			"uid" => $user->uid,
			"threadid" => $thread->id
		);
	}
	else
	{
		// List boards in subarea, thread is not open
		$sql .= "
			AND bt.startpost = true
			AND ba.dir = :dir
		ORDER BY
			bt.sticky DESC,
			bt.bump DESC
		LIMIT :limit";
		$params = array(
			"uid" => $user->uid,
			"dir" => $board->dir,
			"limit" => $board->limitResults
		);
	}
}


$startpostList = $DB->query($sql, $params);

if( !count($startpostList) )
{
	$html->printPageTop();
	echo "No thread(s)";
	$html->printPageBottom();
	exit;
}

if( !$thread->id )
{
	$html->printPageTop( $board->name );

	echo "<span class='borderheader center'>/{$board->dir}/ - {$board->name}";
	if($area["nsfw"])
	{
		echo "&nbsp;<span class='nsfw'>NSFW</span></span>" ;
	}
	else
	{
		echo "&nbsp;<span class='sfw'>SFW</span></span>\n";
	}
	echo "<span class='borderheaderdesc center'>{$board->des}</span>\n<hr />\n";


	echo "<span style='float:right;'>\n";
	echo "\t<a href='/board_list.php' class='button icon-" . (($html->grid) ? "list" : "table2") . "'></a>\n";
	echo "</span>\n";

	if( !in_array( $board->dir, array_keys( $specialAreas ) ) )
	{

		$_MESSAGEFORM["new_thread"] = true;
		require_once("inc/post_form.php");
	}

	echo "<div class='{$board->listtype}'>\n";

	$html->printThread($startpostList);
}
else
{
	$html->printPageTop( $startpostList[0]["topic"] );
	
	echo "<div class='{$board->listtype}'>\n";
	
	$html->printThread($startpostList);
	
	require_once("inc/post_form.php");
	echo "<br />\n<hr class='clear' />\n<br />";
}

if( $access->hasLevel(UserAccessLevel, LEVEL_DEVELOPER) )
{
#	echo "<pre>".$sql."</pre>";
}
echo "\n</div>";

$html->printPageBottom();
$DB->closeConnection();
?>

