<?php
require_once("inc/topinclude.php");
$html = new HTML();
$html->printPageTop( "Register" );

if( $user->registered )
{
	echo $lang->print("ErrorAlreadyLoggedIn");
	$html->printPageBottom();
	exit;
}

echo "\t\t<center>\n";

$html->createForm(
	"reg_post.php",
	array(
		["username", "login_name", "text"],
		["password", "password", "password"],
		["password again", "password2", "password"]
	)
);
echo "\t\t</center>\n";
$html->printPageBottom();
$DB->closeConnection();
?>
