<?php
require_once("inc/topinclude.php");
$id = $_GET["id"] ?? null;

if( $id === null )
	die(lang->ErrorNoIdGiven);

if ( !is_numeric($id) )
	die(lang->ErrorInvalidID);

$line = $DB->row(
	$sql->openPlayerDataQuery,
	[ "id" => $id ],
	fetchmode: PDO::FETCH_OBJ
);

if ( !$line )
	die(lang->ErrorFileNotFound);

$html = new html\HTML();

if ( isset($_GET["close"]) )
{

	$html->printThumbnail(
		dir: $line->filedir,
		filename: $line->filename,
		filetype: $line->filetype,
		action: 'expand',
		messageid: $id,
		blurrable: false,
		nsfw: false,
		embedService: $line->embedservice
	);
	exit;
}

$filedir = $line->filedir;
$thumburl = site->upload_url . "$filedir/t/thumb.avif";
$filetype = $line->filetype;
$fullfileurl = UploadURL . "$filedir/$line->filename";


if(in_array($filetype, ["audio", "video"]))
{
	// I am not sure if this is legal but I assume it's not
	echo "<$filetype id='player_$id' poster='$thumburl' style='width:100%;' controls>\n";
	echo "\t<source src='$fullfileurl' type='$line->mimetype' />\n";
	echo lang->ErrorBrowserAudioOrVideoTag . "\n";
	echo "</$filetype>\n";
}
elseif ($filetype == "flash")
{
	$html->printTag(
		"embed",
		[
			"src" => $fullfileurl,
			"width" => site->embed_width,
			"height" => site->embed_height
		]
	);
}
elseif ($filetype == "embed")
{
	echo preg_replace(
		"/\{code\}/",
		$line->embedcode,
		$embedServices[$line->embedservice]["player"]
	);
}
else
{
		echo lang->ErrorUnknownPlayerType;
}
echo "\n";
$html->printTag(
	"button",
	[
		"data-action" => "closeVideo",
		"data-id" => $id
	],
	inner: lang->HideEmbed
);

$DB->closeConnection();
?>

