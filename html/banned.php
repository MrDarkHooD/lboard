<?php
require_once("inc/topinclude.php");
if( !$user->ban ) {
	header( "Location: $referer" );
	exit;
}

$html = new html\HTML();
$html->printPageTop("Banned");

$line = $DB->row("
SELECT
	to_char(to_timestamp(endtime)::timestamp, 'YYYY-MM-DD\"T\"HH24:MI:SS\"Z\"') as endtime,
	additional_details,
	reason,
	message_id
FROM
	ban
WHERE
	userid = :uid
ORDER BY
	id DESC
LIMIT
	1",
	[ "uid" => $user->uid ]
);

$additional_details = $line["additional_details"];
$endtime = $line["endtime"];

echo "<div class='infobar'>". lang->Banned ."</div>\n";
echo "<img class='righ' src='" . site->static_url . "/img/ban.png' />\n"
echo lang->YouAreBanned . "<br />";
echo lang->BannedNextTimeThink . "<br />\n";
echo lang->BanEnds . " " . $endtime;
echo lang->ReasonYouAreBanned . ":<br />";

echo "<pre>";
echo $line["reason"];
if( $additional_details )
	echo "\n\n".$additional_details;

echo "</pre>";
echo "<br />";

echo lang->MessageThatGotYouBanned . "<br />";

#$HTML->printMessage($line["message_id"];
echo "Message prewiev not available";

echo "But even so, we do not want you to be bored, so go play some <a href='https://archive.org/details/lemmings_original_ms-dos_201705'>Lemmings</a>.";

$html->printPageBottom();
$DB->closeConnection();
?>
