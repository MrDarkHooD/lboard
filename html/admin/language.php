<?php
include('../inc/topinclude.php');

$html = new HTML();
$html->printPageTop( "Language editor" );

if( !$access->hasLevel(UserAccessLevel, LEVEL_DEVELOPER) )
{
	echo $lang->print('ErrorNoRights');
	$html->printPageBottom();
	$DB->closeConnection();
	exit;
}

$lan = $_POST['lang'] ?? "en";
$langList = array (
	"en",
	"fi"
);
echo "<h2>Language editor</h2>\n";
?>

<form action="#" method="post">
  Variable:<input type="text" name="variable">
  <br />
  Result: <input type="text" name="result">
  <br />
	<select name="lang">
<?php
foreach ( $langList as $langCode )
{
	echo "\t\t<option value='$langCode' " . (($lan == $langCode) ? " selected" : "") . ">$langCode</option>\n";
}
?>
	</select>
  <br />
  <input type="submit" name="submit" value="Submit">
</form>

<?php
if( // Yeah..
	isset($_POST['submit']) &&
	isset($_POST['variable']) &&
	!empty($_POST['variable']) &&
	isset($_POST['result']) &&
	!empty($_POST['result']) &&
	in_array($lan, $langList)
)
{
	$DB->query("
INSERT INTO
	lang (
		variable,
		result,
		country
	)
	VALUES (
		:variable,
		:result,
		:country
	)
		",
		array(
			"variable" => $_POST['variable'],
			"result" => $_POST['result'],
			"country" => $_POST['lang']
		)
	);
	echo "Lang added with id " . $DB->lastInsertId();
}

echo "<div class='table' style='width: 1000px;'>\n";
echo "\t<div class='titles'>\n";
echo "\t\t<span>Country</span>\n";
echo "\t\t<span>Variable</span>\n";
echo "\t\t<span>Result</span>\n";
echo "\t</div>\n";

$languages = $DB2->query( "
SELECT
	country,
	variable,
	result
FROM
	lang
ORDER BY
	country,
	variable
	",
	fetchmode: PDO::FETCH_OBJ
);
	foreach($languages as $line)
	{
		echo "\t<div style='border-top: 2px solid black;'>\n";
		echo "\t\t<span>$line->country</span>\n";
		echo "\t\t<span>$line->variable</span>\n";
		echo "\t\t<span>$line->result</span>\n";
		echo "\t</div>\n";
	}

echo "</div>\n";

$html->printPageBottom();
$DB->closeConnection();
?>
