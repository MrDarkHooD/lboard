<?php
include("../inc/topinclude.php");

$html = new HTML();
$html->printPageTop( "Message editor" );

$textHandler = new TextHandler();

if( !$access->hasLevel(UserAccessLevel, LEVEL_ADMIN) )
{
	echo $lang->print('ErrorNoRights');
	$html->printPageBottom();
	exit;
}

if(!isset($_GET['id']))
{
	die("No ID set");
}

$id = $_GET['id'];

$message = $DB->single(
	"SELECT message FROM board_messages WHERE id = ?",
	array($id)
);

if(!$message)
{
	echo("No message found");
	$html->printPageBottom();
	exit;
}

$message = $textHandler->deparseMessage($message);
?>

	<h1>Edit Message</h1>
	<form id="post" action="edit_message_post.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= $id ?>"/>
		<div class="message_form table">
			<div>
				<?= $lang->print('Message'); ?>
			</div>
			<div>
				<textarea
					style="width:99%"
					spellcheck="false"
					autocomplete="off"
					name="message"
					id="message_box"
					class="message_box"
					maxlength="<?= $site->max_message_characters; ?>"
					><?= $message; ?></textarea>
			</div>
			<div class="left">
				<input
					id="postbutton"
					class="button"
					value="Edit Message"
					name="submit"
					type="submit" />
			</div>
		</div>
	</form><br>

<?php
$html->printPageBottom();
$DB->closeConnection();
?>
