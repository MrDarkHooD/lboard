<?php
include("../inc/topinclude.php");

$html = new HTML();
$html->printPageTop( "Site configuration" );

if( !$access->hasLevel(UserAccessLevel, LEVEL_OWNER) )
{
	echo $lang->print('ErrorNoRights');
	$html->printPageBottom();
	exit;
}

echo "<h1>Site settings</h1>";

$query = $DB->row("SELECT * FROM site_settings",array());
echo "<div class=\"container user_settings\">\n";

foreach($query as $key => $value)
{

	// Postgres returns empty values as NULL
	// This is (hopefully) temporary solution;
	// Fixing requires altering table
	// NTM: edit database.sql
	if ($value === NULL)
	{
		$type = "text";
	}
	else
	{
		$type = ["integer" => "number", "string" => "text", "boolean" => "checkbox"][gettype($value)];
	}

	echo "<div>";
	echo "<label for=\"$key\">$key</label>";
	echo "<input type=\"$type\" name=\"$key\" value=\"$value\"";

	echo ($type == "checkbox" && $value) ? " checked" : null;

	echo  "/>\n";
	echo "</div>";
}
echo "</div>";

$html->printPageBottom();
?>
