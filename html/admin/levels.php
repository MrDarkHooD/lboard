<?php
include("../inc/topinclude.php");

$html = new HTML();
$html->printPageTop( "Level editor" );

if( !$access->hasLevel(UserAccessLevel, LEVEL_OWNER) )
{
	echo $lang->print('ErrorNoRights');
	$html->printPageBottom();
	exit;
}

if( isset( $_POST["addLevel"] ) )
{
	$post_fields = [ //[(string)key, (bool)must_isset, (int)type]
		["levelName", true, TYPE_STR, CHARSET_ALPHANUM],
	];

	if( !$post = PostHandler::ValidatePOSTFields($post_fields) )
	{
		echo $lang->print("InvalidPostData") . "<br>";
	}
	else
	{
		$biggest = $DB->single("SELECT MAX(bitwise) FROM levels", array());
		$status = $DB->query("INSERT INTO levels (name, bitwise) VALUES (:name, :bitwise)",
							 array("name" => strtoupper($post->levelName), "bitwise" => $biggest*10));

		$tg = new Telegram();
		$telegramMsg = "$user->username ($user->uid) created new Access level \"{$post->levelName}\".";
		$tg->send($telegramMsg);
		$tg->halt;

	}
}

?>

<form action="?" method="post">
	<b>Add new level</b>
	<input type="text" name="levelName" placeholder="use caps, do not include LEVEL_ prefix"><br />
	<input type="submit" name="addLevel" value="Add new level">
</form>
<br />
<table border=1>
	<tr>
		<th>id</th>
		<th>name</th>
		<th>bitwise</th>
		<th></th>
<?php
$query = $DB->query("SELECT * FROM levels",array());
foreach($query as $line)
{
	echo "\t<tr>\n";
	echo "\t\t<td>{$line["id"]}</td>\n";
	echo "\t\t<td>{$line["name"]}</td>\n";
	echo "\t\t<td>{$line["bitwise"]}</td>\n";
	echo "\t\t<td>";

	$enabledState = ($line["disabled"]) ? '0' : '1';
	$enabledStateChangeText = ($line["disabled"]) ? "Enable" : "Disable";

	echo "\t\t\t<button data-action='changeLevelEnabledState' data-target-id='{$line["id"]}' data-taget-current-state='$enabledState'";

	if( $line["id"] <= 7 )
	{
		echo " disabled";
	}

	echo ">$enabledStateChangeText</button>\n";

}

echo "\t</tr>\n";
echo "</table>\n";

$levelsToGrant = [
	LEVEL_ALL,
    LEVEL_REGISTERED,
	
    LEVEL_MODERATOR,
	LEVEL_ADMIN,
    LEVEL_DEVELOPER,
	LEVEL_OWNER,
	LEVEL_LAWYER
];

#$levelsToGrant = [
#	LEVEL_DEVELOPER
#];

$levels = $access->grant($levelsToGrant);

echo "DEC: " . $levels . "<br>";
echo "BIN: " . decbin($levels)."<br>";

var_dump($access->hasLevel($levels, LEVEL_ALL));
echo "<br>";
var_dump($access->hasLevel($levels, LEVEL_REGISTERED));
echo "<br>";
var_dump($access->hasLevel($levels, LEVEL_PLACEHOLDER));
echo "<br>";
var_dump($access->hasLevel($levels, LEVEL_MODERATOR));
echo "<br>";
var_dump($access->hasLevel($levels, LEVEL_ADMIN));
echo "<br>";
var_dump($access->hasLevel($levels, LEVEL_DEVELOPER));
echo "<br>";
var_dump($access->hasLevel($levels, LEVEL_OWNER));

$html->printPageBottom();
?>

