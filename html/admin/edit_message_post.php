<?php
require_once("../inc/topinclude.php");

if ( !$access->hasLevel( UserAccessLevel, LEVEL_ADMIN ) )
{
	header("Location: {$referer}");
	$DB->closeConnection();
	exit;
}

$postFields = [ //[(string)key, (bool)must_isset, (int)type]
	["id", true, TYPE_INT],
	["message", true, TYPE_STR],
];

if ( !$post = PostHandler::ValidatePOSTFields( $postFields ) )
{
	echo $lang->print("InvalidPostData");
	$DB->closeConnection();
	exit;
}

$textHandler = new TextHandler();

$message = $textHandler->ParseMessage($post->message, $limiter->message);

$query = $DB->single("
SELECT 1
FROM
	board_messages
WHERE
	id = :id
	AND deleted = 'false'
",
	array(
		"id" => $post->id
	)
);

if ( !$query )
{
	$DB->closeConnection();
	die("Message not found");
}

if ( substr_count( $message, "\n" ) > $limiter->enter )
{
	$DB->closeConnection();
	die($lang->print('ErrorTooManyLinebreaks'));
}

$DB->query("UPDATE board_messages SET message = :message WHERE id = :id",
		   array(
			   "message" => $message,
			   "id" => $post->id
		   )
);

Log::Insert("Edited message " . $post->id . ".");
$DB->closeConnection();
header( "Location: /scripts/redirect.php?id=$post->id" );
?>
