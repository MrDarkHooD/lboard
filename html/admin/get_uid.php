<?php
include("../inc/topinclude.php");
if( !$access->hasLevel(UserAccessLevel, LEVEL_ADMIN) )
{
	echo $lang->print('ErrorNoRights');
	$DB->closeConnection();
	exit;
}

if( isset ( $_GET["id"] ) )
{
	echo $DB->single("SELECT uid FROM board_messages WHERE id = ?", array( $_GET["id"] ));

	Log::Insert("Queried poster uid from message " . $_GET["id"]);
}
$DB->closeConnection();
?>
