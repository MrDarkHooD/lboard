<?php
require_once("../inc/topinclude.php");

$html = new HTML();

$html->printPageTop( "Adminpanel" );

if( !$access->hasLevel(UserAccessLevel, LEVEL_MODERATOR) )
{
	echo $lang->print('ErrorNoRights');
	$html->printPageBottom();
	exit;
}
	
echo "<h1>" . $lang->print('Adminpanel') . "</h1>\n";
	
if( $access->hasLevel(UserAccessLevel, LEVEL_OWNER) )
{
	echo "<b>Owner tools</b>";
	echo "<ul>";
	echo "<li><a href=\"site_configuration.php\">" . $lang->print('AdminSiteConfiguration') . "</a></li>\n";
	echo "<li><a href=\"levels.php\">" . $lang->print('AdminSiteAccessConfiguration') . "</a></li>\n";
	echo "</ul>";
}
	
if( $access->hasLevel(UserAccessLevel, LEVEL_DEVELOPER) )
{
	echo "<b>Developer tools</b>";
	echo "<ul>";
	echo "<li><a href=\"errorlog.php\">" . $lang->print('AdminErrorLog') . "</a></li>";
	echo "<li><a href=\"language.php\">" . $lang->print('AdminLanguageEditor') . "</a></li>";
	echo "<li><a href=\"area.php\">" . $lang->print('AdminArea') . "</a></li>";
	echo "<li><a href=\"add_bug.php\">" . $lang->print('AdminBugList') . "</a></li>";
	echo "</ul>";
}

if( $access->hasLevel(UserAccessLevel, LEVEL_ADMIN) )
{
	echo "<b>Admin tools</b>";
	echo "<ul>";
	echo "<li><a href=\"check_ip.php\">" . $lang->print('AdminCheckIP') . "</a></li>";
	echo "<li><a href=\"users.php\">" . $lang->print('AdminUserControl') . "</a></li>";
	echo "</ul>";
}
	
if( $access->hasLevel(UserAccessLevel, LEVEL_MODERATOR) )
{

	echo "<b>Moderator tools</b>";
	echo "<ul>";
	echo "<li><a href=\"lock.php\"> " . $lang->print('AdminLocks') . "</a></li>";
	echo "<li><a href=\"sticy.php\">" . $lang->print('AdminPins') . "</a></li>";
	echo "<li><a href=\"reports.php\">" . $lang->print('AdminReports') . "</a></li>";
	echo "</ul>";
	
}

if( $access->hasLevel(UserAccessLevel, LEVEL_LAWYER) )
{
	echo "<b>Lawyer tools</b>";
	echo "<ul>";
	echo "<li><a href=\"gdpreditor.php\">" . $lang->print('AdminGPDReditor') . "</a></li>";
	echo "</ul>";
}

?>

<pre>
Remember, you are being watched here,
so please, don't do anything stupid.
We know everything you do, and <a href="/scripts/log.php">so do they</a>.
<?= $user->ip; ?>
</pre>
<?php
$html->printPageBottom();
$DB->closeConnection();
?>
