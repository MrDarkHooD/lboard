<?php
require_once("inc/topinclude.php");

if ( $user->registered )
{
	echo lang->ErrorAlreadyLoggedIn;
	exit;
}

$post_fields = [
	["loginname", true, TYPE_STR, CHARSET_ALPHANUM, $filter->allowedSpecial],
	["password", true, TYPE_PASSWORD],
	["password2", true, TYPE_PASSWORD],
	["captcha", false, TYPE_STR]
];

if ( !$post = PostHandler::ValidatePOSTFields($post_fields) )
{
	echo lang->InvalidPostData;
	exit;
}

if (
	$DB->single(
		"SELECT 1 FROM users WHERE login_name = ?",
		array( $post->loginname )
	)
)
{
	echo lang->ErrorUsernameAllreadyTaken;
	exit;
}

if ( $post->password != $post->password2 )
{
	echo lang->ErrorPasswordsDidntMatch;
	exit;
}

$apikey = $DB->single(
	"SELECT apikey FROM users WHERE uid = ?",
	array( $user->uid )
);

$password = password_hash(
	$post->password,
	PASSWORD_ARGON2I
);

$DB->query("
UPDATE
	users
SET
	password = :password,
	login_name = :login_name,
	register_time = CAST(EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) AS INT),
	registered = 1,
	bitwise_level = :bitwise
WHERE
	uid = :uid
	",
	array(
		"password" => $password,
		"login_name" => $post->loginname,
		"bitwise" => $access->grant( [LEVEL_ALL, LEVEL_REGISTERED] ),
		"uid" => $user->uid
	)
);

$user->login(
	$uid,
	$apikey
);

$DB->closeConnection();

header( "Location: /" );
?>
