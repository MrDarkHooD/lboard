<?php
require_once("inc/topinclude.php");

if( !isset($_GET['c']) )
{
	echo "<a href='clearcss.php?c=1'><button>Clear CSS</button></a>";
}
else
{
	$DB->update(
		"users",
		[ "css" => "" ],
		[ "uid" => $user->uid ]
	);

    echo "CSS cleared";
}
$DB->closeConnection();
?>

