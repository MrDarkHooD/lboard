<?php
include("../inc/topinclude.php");

$html = new HTML();
$html->printPageTop( "Admin action log" );

$query = $DB->query("
	SELECT
		action,
		to_char(to_timestamp(time)::timestamp, 'YYYY-MM-DD\"T\"HH24:MI:SS\"Z\"') as time,
		(
			SELECT username
			FROM users
			WHERE
				uid = log.userid
		) AS username
	FROM
		log
	ORDER BY
		id DESC
	LIMIT
		1000
	",
	array());

echo "<h2>Showing 1000 entries.</h2>";
echo "<table border=1 style=\"min-width:800px;\">\n";
echo "<tr>\n";
echo "<th>User nick</th>\n";
echo "<th>Action</th>\n";
echo "<th>Time</th>\n";
echo "</tr>\n";

foreach($query as $line)
{
	echo "<tr>\n";
	echo "<td>{$line['username']}</td>\n";
	echo "<td>{$line["action"]}</td>\n";
	echo "<td>{$line["time"]}</td>\n";
	echo "</tr>\n";
}

echo "</table>";

$html->printPageBottom();
$DB->closeConnection();
?>
