<?php
include("../inc/topinclude.php");

if( !isset($_GET["id"]) )
{
	$DB->closeConnection();
	header("Location: /");
}

$id = $_GET["id"];

$thread = $DB->row("
SELECT
	area.dir AS dir,
	bm.threadid AS id
FROM
	board_messages AS bm
JOIN
	area ON bm.area = area.id
WHERE
	bm.id = ?
	",
	array( $id ),
	fetchmode: PDO::FETCH_OBJ
);

$DB->closeConnection();

header( "Location: /$thread->dir/$thread->id#id$id" );
?>
