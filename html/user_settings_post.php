<?php
require_once("inc/topinclude.php");

if(
	!isset($_POST["submit"]) &&
	!isset($_POST["submit_pwc"]))
{
	header( "Location: /settings/" );
	exit;
}

$postHandler = new handler\Post();

if( isset($_POST["submit_pwc"]) )
{

	if ( !$user->registered )
		die( lang->ErrorYouMustLogIn );

	$post_fields = [
		["currentpw", true, TYPE_PASSWORD],
		["newpw", true, TYPE_PASSWORD],
		["newpw2", true, TYPE_PASSWORD]
	];

	if ( !$post = $postHandler->ValidatePOSTFields($post_fields) )
		die( lang->InvalidPostData );

	if( $post->newpw != $post->newpw2 )
		die( lang->ErrorPasswordsDoentMatch );

	$newpass = password_hash( $post->newpw, PASSWORD_ARGON2I );

	$dbpw = $DB->single(
		"SELECT password FROM users WHERE id = ?",
		[ $user->uid ]
	);

	if ( !password_verify( $post->currentpw, $dbpw ) )
		die( lang->ErrorWrongPassword );

	$DB->update(
		"users",
		[ "password" => $newpass ],
		[ "uid" => $user->uid ]
	);
	$DB->closeConnection();
	header( "Location: /settings/?pass=1" );
	exit;
}

if ( isset($_POST["submit"]) )
{

	$post_fields = [
		["username", true, TYPE_STR, CHARSET_ALPHANUM, $filters->allowedSpecial],
		["actions", true, TYPE_STR, CHARSET_ALPHANUM, [" "]],
		["floating_followed_box", true, TYPE_BOOL],
		["lang", true, TYPE_STR],
		["follow_replied", true, TYPE_BOOL],
		["follow_created", true, TYPE_BOOL],
		["sidebar", true, TYPE_BOOL],
		["theme", true, TYPE_STR],
		["css", true, TYPE_STR],
	];

	if ( !$post = $postHandler->ValidatePOSTFields($post_fields) )
		die( lang->InvalidPostData );

	if ( !empty( $post->username ) )
	{
		$nickTaken = $DB->single(
			"SELECT 1 FROM users WHERE username = :nick AND uid != :uid",
			array(
				"nick"	=> $post->username,
				"uid"	=> $user->uid
			)
		);
		if ( $nickTaken )
			die( lang->PostnameTaken );

		#$DB->insert(
		#	"usernamelog"
		#	array(
		#		"nick" 	=> $post->username,
		#		"ip" 	=> $user->ip
		#	)
		#);
	}

	$DB->update(
		"users",
		array (
			"username"			=> $post->username,
			"default_functions"	=> $post->actions,
			"sidebar"			=> $post->sidebar,
			"style"				=> $post->theme,
			"followed_box"		=> $post->floating_followed_box,
			"follow_created"	=> $post->follow_created,
			"follow_replied"	=> $post->follow_replied,
			"lang"				=> $post->lang,
			"css"				=> $post->css
		),
		array(
			"uid" => $user->uid
		)
	);

	$DB->closeConnection();
	header( "Location: /settings/" );
}
?>
