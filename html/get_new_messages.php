<?php
	require_once("inc/topinclude.php");

$html = new html\HTML();

if ( !$lastid = $_GET["lastid"] )
	die();

$sql = "
SELECT
 	bt.id,
	bt.filedir,
	bt.uid,
	bt.message,
	bt.embed_id,
	bt.username,
	bt.moderator,
	bt.admin,
	bt.floatright,
	bt.sage,
	bt.country,
	bt.mobile,
	bt.tor,
	bt.hideop,
	to_char(to_timestamp(bt.unixtime)::timestamp, 'YYYY-MM-DD\"T\"HH24:MI:SS\"Z\"') as isotime,
	bt.locked,
	bt.sticky,
	bt.threadid,
	bt.tuid,
	bt.file_deleted,
	bt.banned_for_this,
	bt.banned_reason,
	bt.topic,
	bt.startpost,
	ba.dir,
	COALESCE(file.imagetype, NULL) AS filetype,
	COALESCE(file.deleted, NULL) AS og_file_deleted,
	COALESCE(file.filename, NULL) AS filename,
	COALESCE(file.extension, NULL) AS fileextension,
	COALESCE(file.size, NULL) AS filesize,
	COALESCE(file.mimetype, NULL) AS mimetype,
	COALESCE(file.lenght, NULL) AS filelenght,
	COALESCE(file.virus, NULL) AS filevirus,
	(
		SELECT COUNT(id)
		FROM board_messages AS btc
		WHERE
			btc.threadid = bt.id
			AND btc.startpost = false
			AND btc.deleted = false
	) AS reply_count,
	(
		SELECT COUNT(id)
		FROM hidden AS hid
		WHERE
			hid.messageid = bt.id
	) AS hidden_count,
	(
		SELECT COUNT(id)
		FROM followed AS fol
		WHERE
			fol.threadid = bt.threadid
	) AS follow_count,
	(
		SELECT uid
		FROM board_messages AS sp
		WHERE
			sp.id = bt.threadid
	) AS puid,
	(
		SELECT 1
		FROM reports AS rep
		WHERE
			rep.messageid = bt.id
			AND dismissed = false
	) AS reported,
	(
		SELECT ARRAY(
			SELECT messageid
			FROM
				replies as rep
			WHERE
				rep.replies = bt.id
		)
	) AS replies,
	(
		SELECT threadid
		FROM
			board_messages as forced
		WHERE
			forced.id = :lastid
	)
FROM
	board_messages bt
JOIN
	area ba ON bt.area = ba.id
LEFT JOIN
	file ON bt.filedir = file.filedir
WHERE
	bt.deleted = 'false'
	AND bt.id > :lastid
	AND bt.threadid = forced.threadid
ORDER BY bt.id
";

$query = $DB->query($sql, [ "lastid" => $lastid ] );

if($query)
	$newlastid = $html->printThread($query);

$DB->closeConnection();
?>
