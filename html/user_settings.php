<?php
require_once("inc/topinclude.php");
$page->title = $lang->Print('Settings');
require_once("inc/html_top.php");
?>

<h1><?= $lang->print('Settings') ?></h1>
<form id="post" action="/user_settings_post.php" method="post">
	<div class="container user_settings">
		<div>
			<label for="username"><?= $lang->print('SettingsPostername'); ?></label>
			<input type="text" name="username" value="<?= $user->username; ?>">
		</div>
		<div>
			<label for="actions"><?= $lang->print('SettingsMessageActions'); ?></label>
			<input type="text" name="actions" value="<?= $user->functions; ?>">
		</div>
		<div>
			<h3><?= $lang->print('VisualSettings'); ?></h3>
		</div>
		<div>
			<label for="follow_created"><?= $lang->print('SettingsFollowCreated'); ?></label>
			<input type="checkbox" name="follow_created" value="1" <?php if($user->follow_created) echo "checked"; ?>>
		</div>
		<div>
			<label for="follow_replied"><?= $lang->print('SettingsFollowReplied'); ?></label>
			<input type="checkbox" name="follow_replied" value="1" <?php if($user->follow_replied) echo "checked"; ?>>
		</div>
		<div>
			<label for="floating_followed_box"><?= $lang->print('SettingsFollowedFloating'); ?></label>
			<input type="checkbox" name="floating_followed_box" value="1" <?php if($user->followed_box) echo "checked"; ?>>
		</div>
		<div>
			<label for="theme"><?= $lang->print('SettingsLayout'); ?></label>
			<select name="theme">
<?php	
foreach ( $styles as $key => $themename )
{
	echo "\t\t\t\t<option value=\"$themename\"";
	if ( $user->style == $key )
	{
		echo " selected";
	}
	echo ">" . $themename . "</option>\n";
}
?>			</select>
		</div>
		<div>
			<label for="lang"><?= $lang->print('SettingsLanguage'); ?></label>
			<select name="lang">
<?php
for (
	$i=0;
	$i < count($settings->lang);
	$i++
)
{
	echo "\t\t\t\t<option value=\"$languages[$i]\"";
	if($user->lang == $languages[$i])
	{
		echo " selected";
	}
	echo ">" . $settings->lang[$i] . "</option>\n";
}
?>			</select>
		</div>
		<div>
			<label for="sidebar"><?= $lang->print('SettingsShowSidebar'); ?></label>
			<input type="checkbox" name="sidebar" value="1" <?php if($user->sidebar) echo "checked"; ?>>
		</div>
		<!--<div class="fill">
			<b><?php #$lang->print('SettingsPersonalCSS'); ?></b><br />
			<div class="css_fix"><?= $lang->print('SettingsCSSfail'); ?></div>
			<textarea rows="25" cols="50" name="css"><?php # $html->CSS(); ?></textarea>
		</div>-->
		<div>
			<input value="<?php echo $lang->print('SettingsSubmit'); ?>" name="submit" type="submit" />
		</div>
	</div>
</form>
<br /><br />

<?= (isset($_GET["pass"])) ? "<b>".$lang->print('SettingsPasswordUpdated')."</b><br />\n" : ""; ?>

<form id="post" action="user_settings_post.php" method="post">
	<div class="container">
		<div>
			<label for="currentpw"><?= $lang->print('SettingsPasswordCurrent'); ?></label>
			<input type="password" name="currentpw">
		</div>
		<div>
			<label for="newpw"><?= $lang->print('SettingsPasswordNew'); ?></label>
			<input type="password" name="newpw">
		</div>
		<div>
			<label for="newpw2"><?= $lang->print('SettingsPasswordNew2'); ?></label>
			<input type="password" name="newpw2">
		</div>
		<div>
			<input type="hidden" name="pass" value="pass">
			<input type="submit" name="submit_pwc" value="<?= $lang->print('SettingsPasswordSubmit'); ?>">
		</div>
	</div>
</form>
<?php require_once("inc/bottom.php"); ?>