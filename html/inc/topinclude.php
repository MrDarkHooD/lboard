<?php
ini_set('include_path', '/www/html/');

require_once("/www/src/settings.php");
require_once("/www/src/autoloader.php");
//require_once("/www/src/botprevent.php");
require_once("/www/src/sqlQueries.php");

$errorHandler = new handler\Error();

$referer = $_SERVER['HTTP_REFERER'] ?? "/";

$DB = new pdo\Database(
	DBHost,
	DBPort,
	DBName,
	DBUser,
	DBPassword,
	DBEngine
);

$site = $DB->row(
	"SELECT * FROM site_settings",
	fetchmode: PDO::FETCH_OBJ
);
define('site', $site);

$user = new handler\User();
define('user', $user);

if ( !$user->isLoggedIn() )
{
	if ( !$user->createUnregisteredUser() )
	{
		trigger_error(
			"User::createUnregisteredUser() failed.",
			E_USER_ERROR
		);
		exit;
	}

	echo "Wait moment... If nothing happens, refresh this page.";
	header( "Refresh: 0" );
	exit;
}

if ( !$user->initialize() )
{
	trigger_error(
		"User::initialize() failed.",
		E_USER_ERROR
	);
	exit;
}

$access = new handler\Access();
$access->initialize();
define('UserAccessLevel', $user->access_level);

$lang = new handler\Language( $user->lang );
define('lang', $lang);

if(
	$site->maintance &&
	$_SERVER['REQUEST_URI'] != "/login.php" &&
	!$access->hasLevel( UserAccessLevel, LEVEL_DEVELOPER )
)
{
	// Under Construction
	require_once("/www/error/uc.php");
}

if($user->mobile)
{
	$user->sidebar = false;
}

//Last activity tracking to enable deletion of unused accounts
$DB->query("
UPDATE
	users
SET
	last_activity = CAST(EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) AS INT),
	pageloads_count = pageloads_count + 1
WHERE
	uid = ?;
",
   array( $user->uid )
);
?>

