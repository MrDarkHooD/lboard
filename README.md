# LBoard

Imageboard software made originally to https://nyymi.net/ (Finland)  

This software is not ready and is mostly not public (yet).  
Tested on Nginx  

This project was started at 2013 when I was just reached adulthood and some comments and styles tell that very well.
I have lately started to fix everything and this is active project.

### Requirements
* Linux web server
* Postgresql database
* shell_exec() and exec commands (php)
* GD libary
* ffmpeg
* convert
* nice

### What we use
* Debian 12
* nginx 1.22.1
* PHP 8.2
* PostgreSQL 15.5
* Let's encrypt

### Coded with
* html
* css
* js
* php
* SQL
* bash
* python

### Root dirs
* /www/html for main site
* /www/uploads has user uploaded files, this folder is restricted to only allow access to certain filetypes
* /www/static has css, js, fonts and site images
* /www/server has statics from server and user and posting statics
* /www/api has api system that works on its own without need of another directorys
* /www/error/ has all error html documents
* /www/log has error logs
* /www/src has all classes and settings. It also has settings for nginx and table creation etc

Subdomains:
* i for /uploads/
* api for /api/
* server for /hserver/
* static for /static/