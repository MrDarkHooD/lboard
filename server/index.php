<?php
$title = "Index";
include('top.php');


function getSymbolByQuantity($bytes) {
	$symbols = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	$exp = floor(log($bytes)/log(1024));

	return sprintf('%.2f '.$symbols[$exp], ($bytes/pow(1024, floor($exp))));
}
?>
<table style="border:0;width:100%;"><tr><td valign="top">
<center>
	<img src="logo.svg">	
</center>
	
<center>
<h3>Server info</h3>
<table border=1>
	<tr>
		<td>PHP version</td>
		<td><?= phpversion() ?></td>
	</tr>
	<tr>
		<td>OS info</td>
		<td><?= php_uname("s")." ".php_uname("r")." ".php_uname("m")."<br />".php_uname("v"); ?></td>
	</tr>
	<tr>
		<td>SQL version</td>
		<td><?= $DB->single("SELECT VERSION()",array()) ?></td>
	</tr>
	<tr>
		<td>Server software</td>
		<td><?= $_SERVER['SERVER_SOFTWARE'] ?></td>
	</tr>
</table>

<h3>Disk space</h3>
<?php
	$df = disk_free_space("/");
	$ds = disk_total_space("/");

	$percent = round(100-($df/$ds*100), 2);
			
	$du = getSymbolByQuantity($ds-$df);
	$df = getSymbolByQuantity($df);
	$ds = getSymbolByQuantity($ds);
?>	
Total disk space: <?= $ds ?><br />
Disk space left: <?= $df ?><br />
Disk space used: <?= $du ?><br />
Disk space left percentually: <?= (100-$percent) ?>%<br /><br />

<div style="height:50px;width:200px;background-color:green;">
	<div style="height:100%;width:<?= round($percent*2) ?>px;background-color:red;float:left;"></div>
</div>
</center>
	</td><td valign="top">
<center>
<h3>Board stats</h3>
<table border=0 style="width:855px;"><tr><td valign="top">
<?php
	$date = date("Y-m-d");
$database = $DB->query("SELECT COUNT(id) as id, SUBSTRING_INDEX(timestamp, ' ', 1) as date, sessionid as sessionid FROM board_useractivity WHERE SUBSTRING_INDEX(timestamp, ' ', 1) = ? GROUP BY sessionid ORDER BY date DESC",array($date), PDO::FETCH_COLUMN);
	
$today = array_sum($database);
echo "Visitors today: " . $today;
?>
</td><td valign="top" style="float:right;">
<b>messages/area %</b><br><img src="graphs/area.php" />
</td><tr><table>
<b>messages/day from last 30 days</b><br><img src="graphs/message.php" /><br /><br />
<b>Users online hourly started: 13.09.2019</b><br><img src="graphs/online.php" /><br /><br />
</center>
	</td></tr></table>
<?php include('bottom.php'); ?>