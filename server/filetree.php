<?php
function scanner($dir) {
	$name = explode("/", $dir);
	$name = array_reverse($name);
	echo "<li class='folder'>". $name[1] ."</li>";
	$linecount = ["php","css","js","txt","html", "py", "sh"];
	$scan = scandir($dir);
	echo "<ul>";
	foreach($scan as $file) {
		if(!in_array($file, [".",".."])) {
			if(is_file($dir.$file)) {
				$ext = explode(".",$file);
				$ext = end($ext);
				$class = $ext;
				
				$lines = '';
				if(in_array($ext, $linecount)) {
					$handle = fopen($dir.$file, "r");
					while(!feof($handle)){
						fgets($handle);
						$lines++;
					}
					fclose($handle);
					$lines = "($lines)";
				}
				
			}else $class = "folder";
			
			if(is_file($dir.$file)) echo "<li class='$class'>$file $lines</li>";
			if(is_dir($dir.$file)) $dirs[] = $dir.$file.'/';
		}
	}
	foreach($dirs as $dir) scanner($dir);
	echo "</ul>";
}
$title = "Filetree";
include('top.php');
?>
<table border=0>
	<tr>
		<td style="vertical-align:top;">
			<span class="header">html</span>
			<?= scanner("../html/") ?>
		</td>
		<td style="vertical-align:top;">
			<span class="header">src</span>
			<?= scanner("../src/") ?>
		</td>
		<td style="vertical-align:top;">
			<span class="header">static</span>
			<?= scanner("../html_static/") ?>
			</td>
		<td style="vertical-align:top;">
			<span class="header">api</span>
			<?= scanner("../api/") ?>
		</td>
		<td style="vertical-align:top;">
			<span class="header">server</span>
			<?= scanner("../html_server/") ?>
		</td>
		<td style="vertical-align:top;">
			<span class="header">cron</span>
			<?= scanner("../cron/") ?>
		</td>
		<td style="vertical-align:top;">
			<span class="header">error</span>
			<?= scanner("../error/") ?>
		</td>
	</tr>
</table>
<?php include('bottom.php'); ?>