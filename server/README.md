# Server info folder

This folder has files that show different information about server and<br />
board software. It is not recommended to display all these informations<br />
to users. They can give hints to attackers where weak spots are.<br />
<br />
This folder functions on it's own. It requires connection to board database<br />
to fully function, but it does not require any files outside this folder.