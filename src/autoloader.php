<?php
require_once("/www/src/functions.php");

spl_autoload_register(function ($className) {
    $baseDir = '/www/src/';
    $className = str_replace('\\', '/', $className);

    // Load the class file
    $file = $baseDir . $className . '.class.php';
    if (file_exists($file))
        require_once $file;

    $file = $baseDir . $className . '.trait.php';
    if (file_exists($file))
        require_once $file;

});
?>
