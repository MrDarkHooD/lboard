<?php
namespace telegram;

class Telegram {

	private $baseurl;
	private $ch;

	public function __construct() {
		$this->baseurl = "https://api.telegram.org/bot" . telegramAPIkey . "/";
		$this->ch = curl_init();
	}

	public function halt($reason = "")
	{
		curl_close($this->ch);
	}

	public function send($message) {
		$params = [
			'chat_id' => telegramChatId,
			'text' => $message,
			'parse_mode' => 'HTML'
		];

		curl_setopt($this->ch, CURLOPT_URL, $this->baseurl . "sendMessage");
		curl_setopt($this->ch, CURLOPT_POST, true);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($this->ch);

		if ($response === false)
		{
			$error = curl_error($this->ch);

			error_log(
				message: $error . "\n",
				destination: "/www/log/Telegram/errorlog.txt"
			);
		}
	}
}
?>
