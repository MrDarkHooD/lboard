<?php
namespace html;

trait Media {

	public function printThumbnail(
		$dir,
		$action, // redirect, expand
		$messageid,
		$blurrable,
		$nsfw,
		$filename = '',
		$filetype = '',
		$embedService = null
	): void
	{

		$blur = ($blurrable && $nsfw);
		$thumburl = UploadURL . "$dir/t/thumb.avif";

		if( $action == "expand" )
		{
			$fileurl = UploadURL . "$dir/$filename";
		}

		if( !$blur )
		{
			if( $action == "redirect" )
			{
				echo  "<a href='/scripts/redirect.php?id=$messageid'>";
			}

			if( $action == "expand" )
			{

				if ( !in_array($filetype, ["audio","video"] ) )
				{
					$attributes["data-type"] = $filetype;
					$attributes["data-action"] = $action;
				}

				if ($filetype == 'embed')
				{
					$attributes["data-type"] = 'embed';
					$attributes["data-action"] = $action;
					$attributes["class"] = "thumbnail";
					$attributes["data-id"] = $messageid;
					$attributes["data-service"] = $embedService;
				}

				if ($filetype == 'image')
				{
					echo  "<a href='$fileurl' class='expand thumbnail'>";
					$attributes["data-image"] = $fileurl;
					$attributes["class"] = "thumbnail";
				}
				elseif ( in_array($filetype, ["audio","video"] ) )
				{
					$attributes["data-id"] = $messageid;
					$attributes["src"] = $thumburl;
					$attributes["style"] = "width:200px;";

				}
			}
		}

		$attributes["src"] = $thumburl;

		if ( $blur )
		{
			$attributes["class"] = "blur";
			$attributes["data-action"] = "unblur";
			$attributes["data-url"] = "/scripts/redirect.php?id=$messageid";
		}

		echo "<img ";
		foreach ( $attributes as $attr => $value )
		{
			echo "$attr='$value' ";
		}
		echo " />\n";

		if ( in_array($filetype, ["audio", "video", "embed"] ) )
		{
			$playerIcon = "icon-play";

			if( $filetype == "embed" )
				$playerIcon = embedServices[$embedService]["player-icon"];

			echo "\t\t\t<span class='$playerIcon overlay center'></span>\n";
		}

		if( !$blur )
		{
			echo "</a>";
		}
	}
}
?>
