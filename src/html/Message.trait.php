<?php
namespace html;

trait Message {

	private function PrintTags(
		bool $mobile 	= false,
		bool $tor 		= false,
		bool $sage 		= false,
		bool $admin 	= false,
		bool $mod 		= false,
		string $flag 	= null
	): void
	{
		if( $mod )
			echo "<span class=\"mod\">(".$this->lang->print('Moderator').")</span>&nbsp;";

		if( $admin )
			echo "<span class=\"admin\">(".$this->lang->print('Administrator').")</span>&nbsp;";

		if( $flag )
			echo "<img src=\"data:image/png;base64, ".$this->ToBase64($flag)."\" title=\"$flag\" class=\"flag\"/>&nbsp;";

		if ( $mobile )
			echo "<span class=\"icon-mobile\" title=\"MobiFag\"></span>&nbsp;";

		if ( $tor )
			echo "<span class=\"tor\"><img src=\"".site->static_url."img/tor.png\" alt=\"Tor\" title=\"Tor\"></span>&nbsp;";

		if ( $sage )
			echo "<span class=\"sage\"><img src=\"".site->static_url."img/sage.png\" alt=\"Fedora\" title=\"Sage\"></span>&nbsp;";
	}

	private function printTopic (
		int $id,
		string $topic,
		string $directory,
		bool $reported,
		bool $locked,
		bool $stickyed
	): void
	{
		echo "\t\t<span class=\"topic\">\n";
		echo "\t\t\t<a href=\"/" . $directory . "/$id\">\n";

		if(
			$this->access->hasLevel( UserAccessLevel, LEVEL_MODERATOR ) &&
          	$reported
		)
		{
			echo "\t\t\t\t<span class=\"icon-price-tag\" title=\"Reported\"></span>\n";
		}

		if ( $locked )
		{
			echo "\t\t\t\t<span class=\"icon-lock\" title=\"Locked\"></span>\n";
		}

		if ( $stickyed )
		{
			echo "\t\t\t\t<span class=\"icon-pushpin\" title=\"Stickyed\"></span>\n";
		}

		echo $topic . "\n";
		echo "\t\t\t</a>";
		echo "\t\t</span>\n";

	}
	
    public function printThread($messages)
    {

		foreach ( $messages as $message )
        {
			$message2 = (object) $message;

			$id = $message2->id;
			$startpost = $message2->startpost;

			$message2->deleted = false;
			if($message2->deleted)
			{
				if ($startpost)
				{
					echo $this->lang->print('ErrorThreadDeleted');
					break;
				}
				else
				{
					continue;
				}
			}

			$message2->startpost = (bool) $message2->startpost;

			echo "<div class=\"".(($message2->startpost) ? "startpost" : "answer")."\" id=\"message_$id\" data-id='$id'>\n";
			echo "\t<div class=\"postcontainer";
			echo ($message2->hided) ? " hidden" : null;
			echo "\" data-id='$id'>\n";

			if ( $message2->startpost )
			{
				$this->printTopic(
					id: $id,
					topic: $message2->topic,
					directory: $message2->dir,
					reported: (bool) $message2->reported,
					locked: $message2->locked,
					stickyed: $message2->sticky
				);
		    }

			echo "\t\t<div class=\"poster_info\">\n";

		    if(
				$message2->uid == $message2->puid &&
		        !$message2->hideop &&
		        !$message2->startpost &&
		        !$message2->moderator &&
		        !$message2->admin
            )
            {
                echo "\t\t\t<span class=\"op_tag\">".$this->lang->print('OP')."</span>&nbsp;\n";
            }

    		echo "\t\t\t<span class='username'>" . $message2->username . "</span>&nbsp;\n";

			$this->PrintTags(
				mobile: (bool) $message2->mobile, 
				tor: (bool) $message2->tor, 
				sage: (bool) $message2->sage, 
				admin: (bool) $message2->admin, 
				mod: (bool) $message2->moderator, 
				flag: $message2->country
			);

	    	echo "\t\t\t<time>$message2->isotime</time>&nbsp;\n";

		    $selfurl = explode("/",$_SERVER['REQUEST_URI']);
		    if ( empty($selfurl[2]) )
            {
			    echo "\t\t\t<a href=\"/".$message2->dir."/$id\" name=\"id$id\">Nro.</a>&nbsp;\n";
			    echo "\t\t\t<a href=\"/".$message2->dir."/$id\" class=\"messagelink\">$id</a>\n";
		    }
            else
            {
			    echo "\t\t\t<a href=\"#id$id\" name=\"id$id\">Nro.</a>&nbsp;";
			    echo "\t\t\t<a href=\"#textbox\" class=\"messagelink\" data-action=\"quoteMessage\" data-id=\"$id\">$id</a>";
    		}
		    echo "\t\t\t<div class=\"messageoptions\">";
		    if( UserID != $message2->uid )
			{
                echo "\t\t\t\t<a data-action=\"hideMessage\" data-id=\"$id\" class=\"icon-minus\"></a>";
				echo "\t\t\t\t<a data-action=\"report\" data-id=\"$id\" class=\"icon-flag\"></a>&nbsp;";
			}
			else
			{
				echo "\t\t\t<a class=\"icon-delete\" data-action=\"deleteMessage\" data-id=\"$id\"></a>";
			}
		    if( $message2->startpost )
            {

			    echo "\t\t\t\t<a class='icon-enlarge' data-action='expand-all-media'></a>";
			    echo "<span id=\"eye_$id\">";
                echo "<a class=\"".(($message2->followed) ? "icon-eye-blocked" : "icon-eye")."\" data-action=\"followThread\" data-targetId=\"$id\"></a></span>\n";
		    }

            echo "\t\t\t</div>\n";

		    if( $this->access->hasLevel( UserAccessLevel, LEVEL_MODERATOR ) )
            {
                echo "<br><span class=\"messageoptions\">[&nbsp;";
				if($message2->reported)
                {
                    echo "<a class='icon-checkmark' title=\"Dismiss report\"></a>&nbsp;|&nbsp;";
                }

                if($message2->startpost) {
					echo "<a href=\"".URL."admin/move_thread.php?id={$message['threadid']}\" title=\"Move thread\">M</a>&nbsp;|&nbsp;";
					echo "<a class='icon-pushpin' data-action=\"stickyThread\" data-id=\"$id\" title=\"Sticky thread\"></a>&nbsp;|&nbsp;";
					echo "<a class='icon-lock' data-action=\"lockThread\" data-id=\"$id\" title=\"Lock thread\"></a>&nbsp;|&nbsp;";
                }

                echo "<a class='icon-delete' data-action=\"deleteMessage\" data-id=\"$id\" title=\"Delete message\"></a>&nbsp;|&nbsp;";
                if(!$message2->file_deleted && $message2->filedir)
                {
                    echo "<a class='icon-delete' title=\"Delete file\"><span class='icon-attachment'></span></a>&nbsp;|&nbsp;";
                }
                echo "<a class='icon-hammer' href=\"".URL."admin/ban_user.php?messageid={$id}\" title=\"Ban\"></a>&nbsp;";
				if( $this->access->hasLevel( UserAccessLevel, LEVEL_ADMIN ) )
                {
                    echo "|&nbsp;<a href=\"".URL."admin/check_ip.php?id=$id\" title=\"Check IP\">CIP</a>&nbsp;|";
                    echo "<a class='icon-pencil' href=\"".URL."admin/edit_message.php?id={$id}\" title=\"Edit Message\">E</a>&nbsp;";
                }
				echo "]</span>";
            }
			echo "\t\t</div>\n";

		    $embedcode = false;
		    if(
				$message2->filedir ||
				$embedcode
			)
            {
			    $extension = $message2->fileextension;
			    $filename = $message2->filename ?? "";
			    $file = $filename.'.'.$extension;
			    $fileurl = UploadURL.$message2->filedir."/".$file;
			    $thumburl = UploadURL.$message2->filedir."/t/thumb.avif";
		    }

		    if($message2->filedir) {

				if ( $message2->file_deleted )
            	{
	    			echo "\t\t<div class=\"thumb left filecontainer\">\n";
                	echo "\t\t\t<img src=\"".MediaURL."img/file_deleted.png\" alt=\"Deleted\">\n";
					echo "\t\t</div>";
    			}
				else
				{

					$float = ($message['floatright']) ? "right" : "left";

					echo "\t\t<div class=\"$float thumb filecontainer\">\n";
					echo "\t\t\t<span class=\"data_info\">$filename (<a href=\"$fileurl\">$extension</a>, ".$message2->filesize.")</span><br />\n";

					if( $extension === "swf" )
					{
						$thumburl = MediaURL."img/swf.png";
					}

					if ( $this->grid )
					{

						$this->printThumbnail(
							dir: $message2->filedir,
							filename: $file,
							filetype: '',
							action: 'redirect',
							messageid: $id,
							blurrable: false,
							nsfw: false
						);

					}
					elseif( in_array($message2->filetype, ["image", "audio","video"]) )
					{

						$this->printThumbnail(
							dir: $message2->filedir,
							filename: $file,
							filetype: $message2->filetype,
							action: 'expand',
							messageid: $id,
							blurrable: false,
							nsfw: false
						);

					}
					elseif( $extension === "swf" )
					{
						echo "\t<div id=\"video_$id\">\n";
						echo "\t\t<a data-action=\"expand-flash\" data-id=\"$id\">\n";
						echo "\t\t\t<img src=\"".MediaURL."img/swf.png\" />\n";
						echo "\t\t</a>\n";
						echo "\t</div>\n";
					}
					elseif ( in_array( $extension, ["zip", "rar", "7z"] ) )
					{
						echo "\t<a href=\"$fileurl\" class=\"icon-file-zip\" target=\"_blank\"></a>\n";
					}
					elseif ( $extension == "pdf" )
					{
						echo "\t<a href=\"$fileurl\" class=\"icon-file-pdf\" target=\"_blank\"></a>\n";
					}

					if ( $message2->filevirus )
					{
						echo "\t<span class=\"virus\">VIRUS!!!<br />VIRUS!!!<br />VIRUS!!!</span>\n";
					}
					echo "</div>\n";

		    	}
    		}
			/*
            elseif(!$filedata && $embedcode)
            {

			    $embeddata = $this->DB->row("SELECT * FROM embed WHERE code = ?", array($embedcode));
                $ogLink = $embedService["video_baseurl"] . $embedcode;
                $embedService = embedServices[$message['embed_type']];
            
    			echo "<div id=\"video_{$id}\" class=\"filecontainer {$float} thumb\">";
	    		echo "<span class=\"data_info\">";
                echo "<a href=\"{$ogLink}\">" . $embeddata['title'] . "</span><br \>\n";

    			echo ($grid) ? "<a href=\"".URL.$message["dir"]."/$id\">\n" : "<a onclick=\"videoexpander($id)\">\n";
            
                $playerIco = ($embedService["player-icon"]) ?? "icon-play3";
                $serviceIco = ($embedService["service-icon"]) ?? null;
            
                echo "<img src=\"{$thumburl}\" width=\"200px\">\n";
                echo "<span class=\"{$playerIco} overlay center {$serviceIco}\"></span>\n";
	    		echo "</a><span class=\"overlay bottom right\">" . $embeddata['duration'] . "</span></div>"; //filecontainer
		    }*/

			echo "<div class=\"message\">\n";

			#echo message_parser($message2->message, bbfrom, bbto);
			echo $message2->message;

			if( $message2->banned_for_this )
			{
				echo "<br />\n\t<span class=\"red bold\">(" . $this->lang->print('UserWasBannedForThis') . ": ".$message2->banned_reason.")</span>\n";
			}

		echo "\t\t</div>"; //message
		echo "\t</div>"; //postcontainer

		// Print replies
		$replies = explode( ',', str_replace( ['{', '}'], '', $message2->replies) );

		if ( $replies[0] != '' )
		{
			echo "<span class=\"omitted replies\">".$this->lang->print('Answears').": \n";
			foreach ( $replies as $reply )
			{
				echo "\t<a href='#id$reply' class='messageReference' data-action='hoverQuote' data-id='{$reply}'>>>$reply</a>&nbsp;\n";
			}
			echo "</span>\n";
		}

		if ( $message2->startpost )
		{

			echo "<span class=\"omitted\">" . $this->lang->print("ThreadReplyCount") . " " . $message2->reply_count;

			if( UserID == $message2->uid )
			{
                echo " " . $this->lang->print("ThreadHideCount") . " " . $message2->hidden_count . " Followers " . $message2->follow_count;
			}

			echo "</span>";

		}

		echo "</div>";

		} //foreach
	return $messages[count($messages)-1]["id"];
	}
}

