<?php
namespace html;
use PDO;
use \pdo\Database;
use \handler\Access;

class HTML {
	use MessageTrait;
	use MediaTrait;
	use Flag;

	private $lang;
	private $DB;
	private $access;
	private $subBoards = array( );
	private $title;

	public function __construct()
	{
		$this->DB = new Db(
			DBHost,
			DBPort,
			DBName,
			DBUser,
			DBPassword,
			DBEngine
		);
		
		$this->lang 	= new Language( UserLang );
		$this->access 	= new Access();
	
		$this->title = site->name;

		$boards = $this->DB->query("
			SELECT name,
				nsfw,
				dir,
				bitwise_level
			FROM
				area
			WHERE
				hidden = 'false'
			ORDER BY
				dir
		",
			array() );

		foreach ( $boards as $board )
		{

			if ( !$this->access->hasLevel( UserAccessLevel, $board["bitwise_level"]) )
			{
				continue;
			}
			
			array_push($this->subBoards, (object) array (
				"dir" 	=> $board["dir"],
				"name" 	=> $board["name"],
				"nsfw" 	=> $board["nsfw"]
			) );
		}
		
	}
	
	private function updateTitle( $newTitle = null ): void
	{
		$this->title = ($newTitle) ? $newTitle . " - " . site->name : site->name;
	}
	
	public function htmlBoardList( $dir = 0 )
	{
		echo "<ul>\n";
		echo "\t<li><a href=\"".URL."overboard/\">Overboard</a></li>\n";
		echo "\t<li><a href='/overboard/' data-dir='overboard' data-nsfw='0'>Overboard</a></li>\n";
		foreach( $this->subBoards as $area )
		{
			echo "\t<li><a href='/{$area->dir}/' data-dir='$area->dir' data-nsfw='$area->nsfw'>$area->name</a>";
		}
		echo "</ul>\n";
	}
	
	public function printNavHeader()
	{
		echo "<div class='container logo'>\n";
		echo "\t<a href='/'>\n";
		echo "\t\t<img src='" . site->logo_url . "' class='logo' />\n";
		echo "\t</a>\n";
		echo "\t<a href='/'>\n";
		echo "\t\t" . site->name . "\n";
		echo "\t</a>\n";
		echo "</div>\n";
	}
	
	public function printNavList()
	{
		echo "<ul>\n";
		echo "\t<li class=\"header\">" . $this->lang->print('Navigation') . "</li>\n";
		$this->printLinkGDPR();
		echo "\t<li><a href=\"/followed/\">" . $this->lang->print('FollowedThreads') . "</a></li>\n";
		echo "\t<li><a href=\"/own/\">" . $this->lang->print('OwnThreads') . "</a></li>\n";
		echo "\t<li><a href=\"/replied/\">" . $this->lang->print('RepliedThreads') . "</a></li>\n";
		echo "\t<li><a href=\"/hidden/\">" . $this->lang->print('HiddenThreads') . "</a></li>\n";
		echo "\t<li><a href=\"/settings/\">" . $this->lang->print('Settings') . "</a></li>\n";
		echo "</ul>\n";
		echo "<ul>\n";
		
		if ( !user->registered )
		{
			echo "\t<li class=\"header\">" . $this->lang->print('LogIn') . "</li>\n";

			echo "\t<li><a href=\"/register.php\">" . $this->lang->print('Register') . "</a></li>\n";
			echo "\t<li><a href=\"#\" data-action='openLoginForm'>" . $this->lang->print('LogIn') . "</a></li>\n";
			echo "</ul>\n";
			
			$this->loginForm();
		}
		else
		{
			echo "\t<li>" . $this->lang->print('LoggedInAs') . " " . user->login_name . "</li>\n";
			echo "\t<li><a href=\"#\" data-action=\"logout\">" . $this->lang->print('LogOut') . "</a></li>\n";
		
		
			if ( $this->access->hasLevel( UserAccessLevel, LEVEL_MODERATOR ) )
			{
				echo "\t<li><a href=\"/admin/\">".$this->lang->print('Adminpanel')."</a></li>\n";
				echo "\t<li><a href=\"https://static.nyymi.net/css/style.css\">style.css</a></li>\n";
			}
			echo "</ul>\n";
		}
	}
	
	# The fuck is this shit? Table in menu, mixed with list.
	public function printLinkGDPR()
	{
		$fileLocation = "/www/src/GDPR.txt";
		$editTime = date("Y-m-d", filemtime($fileLocation));
		
		echo "\t<li><a href=\"/index.php?a=gdpr\"";
		if ( $editTime > date( 'Y-m-d', strtotime("-1 week") ) )
		{
			echo " class=\"gpdrupdated\" ";
		}
		echo ">GDPR</a></li>\n";
	}

	public function FollowedBox()
	{
		echo "\t\t<div id='followed_box' class='followed_box'>\n";
		echo "\t\t\t<span class='title'>" . $this->lang->print('FollowedThreads') . "</span>\n";
		echo "\t\t\t<div class='followed_threads' id='followed_threads'></div>\n";
		echo "\t\t\t<div class='followedoptions'>\n";
		echo "\t\t\t\t<a data-action='hideFollowed' class='icon-minus'></a>\n";
		echo "\t\t\t\t<a data-action='updateFollowed' class='icon-refresh'></a>\n";
		echo "\t\t\t</div>\n";
		echo "\t\t</div>\n";
	}

	public function loginForm()
	{
		echo "<form id='post' action='/login.php' method='post'>\n";
		echo "\t<div class='container'>\n";
		echo "\t\t<div class='fill'>\n";
		echo "\t\t\t<input type='text' name='username' placeholder='" . $this->lang->print("Username") . "' />\n";
		echo "\t\t\t<input type='password' name='password' placeholder='" . $this->lang->print("Password") . "' />\n";
		echo "\t\t\t<input type='submit' name='submit' value='" . $this->lang->print("LogIn") . "' />\n";
		echo "\t\t</div>\n";
		echo "\t</div>\n";
		echo "</form>\n";
	}
	
	private function printUserCSS(): void
	{

		if ( !empty( user->css ) )
		{
			echo "\t<style>\n";
			echo user->css;
			echo "\n\t</style>\n";
		}

	}
	
	public function printHead( )
	{
		echo "\t<head>\n";
		echo "\t\t<title>" . $this->title . "</title>\n";
		echo "\t\t<link rel=\"preconnect\" href=\"\">\n";
		echo "\t\t<link rel=\"preload\" href=\"" . site->logo_url . "\" as=\"image\">\n";
		echo "\t\t<meta name=\"robots\" content=\"index, follow, noarchive\">\n";
		echo "\t\t<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />\n";
		echo "\t\t<meta property=\"og:title\" content=\"$this->title\">\n";
		echo "\t\t<meta name=\"og:description\" content=\"\">\n";
		echo "\t\t<meta property=\"og:image\" content=\"" . site->logo_url . "\">\n";
		#echo "\t\t<meta name=\"keywords\" content=\"$meta->keywords\">\n";
		#echo "\t\t<meta name=\"author\" content=\"$meta->author\">\n";
		# <meta name="viewport" content="width=device-width, initial-scale=1.0">
		echo "\t\t<link rel=\"icon\" type=\"image/ico\" href=\"//". site->static_url . "/img/icon.png\" />\n";

		$styles = [
			"icons.css",
			"icomoon/style.css",
			"general.css",
			user->theme . ".css",
			"highlight.css"
		];
		
		foreach ( $styles as $style )
		{
			echo "\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"//" . site->static_url . "/css/$style?a=".time()."\" />\n";
		}

		$this->printUserCSS();

		$scripts = [
			"notificationBubbles.js",
			"expand.js",
			"hover.js",
			"post.js",
			"highlight.min.js",
			"js.js"
		];
		
		foreach ( $scripts as $script )
		{
			echo "\t\t<script src=\"//" . site->static_url . "/js/$script\" defer></script>\n";
		}


		echo "\t\t<script src=\"//instant.page/5.2.0\" type=\"module\" integrity=\"sha384-jnZyxPjiipYXnSU0ygqeac2q7CVYMbh84q0uHVRRxEtvFPiQYbXWUorga2aqZJ0z\"></script>\n";
		echo "\t</head>\n";
	}
	
	public function printPageTop( $title )
	{
		$this->updateTitle( $title );
		echo "<!DOCTYPE html>\n";
		echo "<html lang='" . user->lang . "'>\n";
		$this->printHead();
		echo "\t<body data-instant-intensity='mousedown'";
		echo (!user->sidebar) ? " class='no_sidebar'" : null;
		echo ">\n";
		echo "\t\t<div id='notifications' class='notifications'></div>\n";
		
		# Navigation related, url.ext#top
		echo "<div id='top'></div>\n";
		
		# Nav bar
		echo "\t\t<div class='nav";
		#echo (!user->sidebar) ? " no_sidebar" : null;
		echo "'>\n";
		echo "<div class='nav_margin'></div>";
		echo "<div class='nav_content'>";
		
		$this->printNavHeader();
		$this->printNavList();
		$this->htmlBoardList();
		echo "</div>\n";
		echo "\t\t</div>\n";
		echo "<div class='content'>\n";
	}
	
	public function printPageBottom()
	{
		echo "\t\t\t<footer>\n";
		echo "\t\t\t\t<div>\n";
		echo $this->lang->print('Copyright') . " ";
		echo site->meta_author . " ";
		echo site->startyear . " - ";
		echo date("Y") . "\n";
		echo "\t\t\t\t</div>\n";
	
		echo "\t\t\t\t<div>\n";
		echo $this->lang->print('BottomCookieText');
		echo "\t\t\t\t</div>\n";

		echo "<div style='white-space:pre;'>\n";
		echo "<a href='https://server.nyymi.net/'>Server info</a>\n";
		echo $this->lang->print('HostProvider') . " " . site->serverprovider . "\n";
		echo $this->lang->print('DomainProvider') . " " . site->domainprovider . "\n";
		echo "Powered by <a href='https://gitlab.com/MrDarkHooD/lboard' target='_blank'>LBoard</a>\n";
		echo round( ( microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'] ), 3 ) . "s\n";
		echo "\t\t\t\t</div>\n";
		echo "\t\t\t</footer>";

		echo "\t\t</div>";
		echo "\t</body>\n";
		echo "</html>\n";
	}

	public function createForm(
		$target,
		$fields,
		$submit = ["Submit", "submit"],
		$width = '500px'
	)
	{

		echo "<form action='$target' method='post'>\n";
		echo "\t<div class='table' style='width:$width;'>\n";

		foreach ($fields as $data)
		{
			list($label, $name, $type, $value) = array_pad( $data, 4, null );

			if(
				$label == null ||
				$name == null
			)
			{
				trigger_error(
					"\$fields array in array input to createForm() must have str value in first and second field",
					E_USER_ERROR
				);
				exit;
			}

			$type = $type ?? "text";

			echo "\t\t<div>\n";
			echo "\t\t\t<label for='$name'>$label</label>\n";
			echo "\t\t\t<input\n\tname='$name'\n\ttype='$type'\n\tvalue='$value'\n\tplaceholder='$label' />\n";
			echo "\t\t</div>\n";
		}
		echo "\t\t<div>\n";
		echo "\t\t\t<input name'".$submit[1]."' type='submit' value='".$submit[0]."'/ />\n";
		echo "\t\t</div>\n";
		echo "\t</div>\n";
		echo "</form>\n";
	}

}
