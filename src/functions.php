<?php

	function multineedle_stripos($haystack, $needles) {
		foreach ($needles as $letter)
			if(stripos($haystack,$letter) !== false)
				return true;
		return false;
	}

	//https://gist.github.com/kamermans/1548922
	function DNSBL($ip, $server, $timeout=1) { //ipInDnsBlacklist('188.163.68.29', 'dnsbl.tornevall.org');
		$response = [];
		$host = implode(".", array_reverse(explode('.', $ip))).'.'.$server.'.';
		$cmd = sprintf('nslookup -type=A -timeout=%d %s 2>&1', $timeout, escapeshellarg($host));
		@exec($cmd, $response);
		for ($i=3; $i<count($response); $i++) { // The first 3 lines (0-2) of output are not the DNS response
			if(strpos(trim($response[$i]), 'Name:') === 0) return true;
		}
		return false;
	}

	function URLexist($file) {
		$file_headers = @get_headers($file);
		if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found')
    		return false;
    	return true;
	}

	//https://stackoverflow.com/questions/677419/how-to-detect-search-engine-bots-with-php
	function bot_detected() {
		return(isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT']));
	}

	//https://www.php.net/manual/en/function.disk-total-space.php
	function getSymbolByQuantity($bytes) {
    	$symbols = array('B','KB','MB','GB','TB','PB','EB','ZB','YB');
    	$exp = floor(log($bytes)/log(1024));
    	return sprintf('%.2f '.$symbols[$exp],($bytes/pow(1024,floor($exp))));
	}
