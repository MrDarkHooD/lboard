<?php
namespace handler;
use Imagick;

define("UPLOAD_ERR_SOFT_SIZE", 9);
define("UPLOAD_ERR_FORBIDDEN_MIMETYPE", 10);
define("UPLOAD_ERR_FORBIDDEN_EXTENSION", 11);
define("UPLOAD_ERR_MALICIOUS_FILENAME", 12);

class File {

	public int $nice;
	
	public static function UploadError( $error )
	{
		$return = [];
		switch($error) {
			case 1:
			case UPLOAD_ERR_INI_SIZE:
				$return["title"] = "Filesize too big (ini).";
				$return["description"] = "The uploaded file exceeds the upload_max_filesize (".ini_get('upload_max_filesize').") directive in php.ini.";
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_INI_SIZE";
				break;
				
			case 2:
			case UPLOAD_ERR_FORM_SIZE:
				$return["title"] = "Filesize too big (form).";
				$return["description"] = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.";
				$return["type"] = "warning";
				$return["code"] = "UPLOAD_ERR_FORM_SIZE";
				break;
				http_response_code(413); // Content Too Large
			case 3:
			case UPLOAD_ERR_PARTIAL:
				$return["title"] = "File did not upload fully";
				$return["description"] = "The uploaded file was only partially uploaded.";
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_PARTIAL";
				break;
				
			case UPLOAD_ERR_NO_FILE:
			case 4:
				$return["title"] = "No file was uploaded.";
				$return["description"] = "There is no uploaded file on server.";
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_NO_FILE";
				break;
				
			case 6:
			case UPLOAD_ERR_NO_TMP_DIR:
				$return["title"] = "/tmp/ missing";
				$return["description"] = "Missing a temporary folder.";
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_NO_TMP_DIR";
				Log::InsertError(
					errorMessage: "Temporary folder for file uploads is missing\r\n" . print_r($error, true),
					formatted: false,
					urgent: true,
					onlyMail: false,
					type: FATAL
				);
				http_response_code(507); // Insufficient Storage
				break;
				
			case 7:
			case UPLOAD_ERR_CANT_WRITE:
				$return["title"] = "Write fail";
				$return["description"] = "Failed to write file to disk.";
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_CANT_WRITE";
				Log::InsertError(
					errorMessage: "For some reason PHP was unable to write uploaded file into disk.\r\n" . print_r($error, true),
					formatted: false,
					urgent: true,
					onlyMail: false,
					type: FATAL
				);
				http_response_code(507); // Insufficient Storage
				break;
				
			case 8:
			case UPLOAD_ERR_EXTENSION:
				$return["title"] = "A PHP extension stopped the file upload";
				$return["description"] = "PHP does not provide a way to ascertain which extension caused the file upload to stop";
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_EXTENSION";
				Log::InsertError(
					errorMessage: "Something external is teasing fileupload.\r\n" . print_r($error, true),
					formatted: false,
					urgent: true,
					onlyMail: false,
					type: FATAL
				);
				break;
				
			// Self defined
			case 9:
			case UPLOAD_ERR_SOFT_SIZE:
				$return["title"] = "Filesize too big";
				$return["description"] = "File you tried to upload is bigger than we allow.";
				//$lang->print('ErrorUploadFileTooBig')
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_EXTENSION";
				break;
				
			case 10:
			case UPLOAD_ERR_FORBIDDEN_MIMETYPE:
				$return["title"] = "";
				$return["description"] = "";
				//$lang->print("ErrorUploadMimetypeError");
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_FORBIDDEN_MIMETYPE";
				http_response_code(415); // Unsupported Media Type
				break;
			case 11:
			case UPLOAD_ERR_FORBIDDEN_EXTENSION:
				$return["title"] = "";
				$return["description"] = "";
				//$lang->print("ErrorUploadImagetypeNotAllowed")
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_FORBIDDEN_EXTENSION";
				http_response_code(415); // Unsupported Media Type
				break;
				
			case 12:
			case UPLOAD_ERR_MALICIOUS_FILENAME:
				$return["title"] = "";
				$return["description"] = "";
				//$lang->print("ErrorUploadFileNameMalicious")
				$return["type"] = "error";
				$return["code"] = "UPLOAD_ERR_MALICIOUS_FILENAME";
				http_response_code(415); // Unsupported Media Type
				break;
				
			default:
				$return["title"] = "Unknown file upload error";
				$return["description"] = "This should not happen, like seriously. We'll look into this.";
				$return["type"] = "error";
				$return["code"] = "?";
				$return["realname"] = "UPLOAD_ERR_?";
				Log::InsertError(
					errorMessage: "Something happened in file upload, but it's something nobody was prepared for.\r\n" . print_r($error, true) . "\r\n" . print_r($_FILES, true),
					formatted: false,
					urgent: true,
					onlyMail: false,
					type: FATAL
				);
				http_response_code(418); // I'm a teapot
				break;
		}
		return $return;
	}
	
	public static function RemoveExif( $location )
	{
		$img = new Imagick($location);
		$orientation = $img->getImageOrientation();
		$img->stripImage();
		$img->setImageOrientation($orientation);
		$img->writeImage($location);
		return;
	}
	
	public function GetData( $location )
	{
		$file = [];
		$temp				= explode(".", $location);
		$file['extension']	= strtolower(end($temp));
		$file['size']		= filesize($location);
		$file['name'] 		= $location;
		$cmdinput 			= escapeshellarg($location);
		
		$file['name'] = explode("/",$file['name']);
		$file['name'] = end($file['name']);
		
		if ( in_array( $file["extension"], ["jpg", "jpeg", "png", "gif", "webp", "avif"]) )
		{
			$file["imagetype"] = "image";
			$file['mimetype'] = getimagesize($location)['mime'];
			$file['imagetype'] = exif_imagetype($location);
			
			list($width, $height, $type, $attr) = getimagesize($location);
			$file['width'] = $width;
			$file['height'] = $height;
			if($file['extension'] == "gif")
			{
				$file['frames'] = shell_exec("nice --adjustment=$this->nice identify $cmdinput | wc -l");
			}
		}
		elseif ( in_array( $file["extension"], ["mp4", "webm"] ) )
		{
			$file["imagetype"] = "video";
			$probe = $this->probeVideo( $cmdinput );
			$file = array_merge($file, $probe);
		}
		elseif ( in_array( $file["extension"], ["mp3", "wav"] ) )
		{
			$file["imagetype"] = "sound";
			$probe = $this->probeAudio( $cmdinput );
			$file = array_merge($file, $probe);
		}

		return $file;
	}
	
	private function probeVideo( $cmdinput )
	{
		$probe = shell_exec("nice --adjustment=$this->nice ffprobe -show_streams -of json $cmdinput -v quiet");
		$videoInfo = json_decode($probe);
		$videoInfo = $videoInfo->streams[0];
		
		$file['width'] = $videoInfo->width;
		$file['height'] = $videoInfo->height;
		$file['duration'] = round(($videoInfo->duration ?? 0), 2);
		
		return $file;
	}
	
	private function probeAudio( $cmdinput )
	{
		$probe = shell_exec("nice --adjustment=$this->nice ffprobe -show_streams -of json $cmdinput -v quiet");
		$audioInfo = json_decode($probe);
		$audioInfo = $videoInfo->streams[0];

		$file['duration'] = round($audioInfo->duration, 2);
		
		return $file;
	}
	
	public function MakeThumbnail(
		string $input,
		string $output,
		$type,
		int $quality = 30,
		int $width = 100
	)
	{
		$cmdinput = escapeshellarg($input);
		$cmdoutput = escapeshellarg($output);
		$cmdwidth = escapeshellarg($width);
		switch($type)
		{
			case "image":
				
				/* TODO:
			 	 * - Figure out safety of svg and implement it
			 	 */
				
				$im = self::thumbnail($input, $width);

				try
				{
					imageavif($im, $output);
				}
				catch( Exception $e )
				{
					/* TODO:
					 * - Add logging for future debugging and improving
					 * $e->getMessage();
					 */
					
					copy('/www/static/img/file_nothumb.png', $output);
				}
				break;

			case "audio":
				exec("nice --adjustment=$this->nice ffmpeg -i '$cmdinput' '$cmdoutput'");
				exec("nice --adjustment=$this->nice avifenc '$cmdoutput' -o '$cmdoutput'");
				break;
				
			case "video":
				exec("nice --adjustment=$this->nice ffmpegthumbnailer -i '$cmdinput' -o '$cmdoutput' -s $cmdwidth -ts 00:00:01.000");
				exec("nice --adjustment=$this->nice avifenc '$cmdoutput' -o '$cmdoutput'");
				break;
				
			default:
				die(json_encode(["err" => "Failed to generate thumbnail"]));
				break;
		}
		return 0;
	}
	
	private static function thumbnail($inputFileName, $maxSize = 200) {
        $info = getimagesize($inputFileName);
        $type = isset($info['type']) ? $info['type'] : $info[2];
            if(!(imagetypes() & $type)) {
                return false;
            }

        $width  = isset($info['width'])  ? $info['width']  : $info[0];
        $height = isset($info['height']) ? $info['height'] : $info[1];
 
        $wRatio = $maxSize / $width;
        $hRatio = $maxSize / $height;
 
        $sourceImage = imagecreatefromstring(file_get_contents($inputFileName));
		if(!$sourceImage) $sourceImage = imagecreatefromwebp($inputFileName);
 
            if(($width <= $maxSize) && ($height <= $maxSize)) {
                return $sourceImage;
            }elseif(($wRatio * $height) < $maxSize) {
              $tHeight = ceil($wRatio * $height);
              $tWidth  = $maxSize;
            }else {
              $tWidth  = ceil($hRatio * $width);
              $tHeight = $maxSize;
            }
 
        $thumb = imagecreatetruecolor($tWidth, $tHeight);
		$white = imagecolorallocate($thumb, 255, 255, 255); 
          imagefill($thumb,0,0,$white); 
 
            if($sourceImage == false) {
                return false;
            }
 
          imagecopyresampled($thumb, $sourceImage, 0, 0, 0, 0, $tWidth, $tHeight, $width, $height);
          imagedestroy($sourceImage);
 
          return $thumb;
      }

	//https://www.phpclasses.org/package/10234-PHP-Rotate-images-automatically-based-on-orientation.html
	private static function reflejarImagen ($imageOriginal) {
		$width = imagesx ($imageOriginal);
		$height = imagesy ($imageOriginal);

		$origenDeX = $width -1;
		$origenDeY = 0;
		$width_original = -$width;
		$height_original = $height;

		$destinationImage = imagecreatetruecolor ($width, $height);

		if (imagecopyresampled ($destinationImage, $imageOriginal, 0, 0, $origenDeX, $origenDeY, $width, $height, $width_original, $height_original)) return $destinationImage;

		return $imageOriginal;
	}
	 
	public static function RemoveRotation($file) {
		$exif = exif_read_data($file);
		/* We determine the image format through Exif itself. */
		$format = exif_imagetype($file);
		if($exif && isset($exif['Orientation'])) {
			$orientation = $exif['Orientation'];
			if($orientation != 1){
				$im = imagecreatefromjpeg($file);

				$reflection = false;
				$degrees = 0;
				switch ($orientation) {
					case 2:
						$reflection = true;
						break;
					case 3:
						$degrees = 180;
						break;
					case 4:
						$degrees = 180;
						$reflection = true; 
						break;
					case 5:
						$degrees = 270;
						$reflection = true; 
						break;
					case 6:
						$degrees = 270;
						break;
					case 7:
						$degrees = 90;
						$reflection = true; 
						break;
					case 8:
						$degrees = 90;
						break;
				}
				if ($degrees) $im = imagerotate($im, $degrees, 0); 
				if ($reflection) $im = self::reflejarImagen($im);

				imagejpeg($im, $file);
			}
		}
		return true;
	}
}
