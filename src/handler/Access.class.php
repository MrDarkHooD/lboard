<?php
declare(strict_types=1);
namespace handler;
use PDO;
use \pdo\Database;

/* Thanks to TosiReiska from Ylilauta */
class Access {

	public $DB;

    public function __construct()
	{

		$this->DB = new Database(
			DBHost,
			DBPort,
			DBName,
			DBUser,
			DBPassword,
			DBEngine
		);
	}

	public function initialize(): void
	{

		$levels = $this->DB->column("
SELECT CONCAT('LEVEL_', UPPER(name)) AS name,
	bitwise
FROM
	levels
WHERE
	disabled = false
ORDER BY
	id ASC
			",
			fetchmode: PDO::FETCH_KEY_PAIR
		);

		foreach ( $levels as $level => $bitwise )
		{

			if( defined($level) )
			{
				trigger_error(
					"Initializing access levels failed, constant(s) already defined.",
					E_USER_ERROR
				);
				exit;
			}

			define(
				$level,
				$bitwise
			);
		}
	}

	private static function isLevel( $var ): bool
	{
		if ( !defined( $var ) )
		{
			return false;
		}

		$bin = decbin($var);
		return (substr($a, 0, 1)==1 && !round(substr($a, 1))) ? true : false;
	}

	private static function indexToLevel( int $index ): array
	{
		$int = $index-1^2;
		$bin = decbin( $int );
		return ["int" => $int, "bin" => $bin];
	}

    public static function hasLevel(
		int $accessValue,
		int $level
	): bool
	{
        return ($accessValue & $level) === $level;
    }

    public static function grant( array $levels ): int
	{
        $combinedValue = 0;
        foreach ($levels as $level)
		{
            $combinedValue |= $level;
        }
        return $combinedValue;
    }
}
?>
