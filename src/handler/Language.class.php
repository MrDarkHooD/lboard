<?php
namespace handler;
use PDO;

class Language {
	private $lang;
	private $arr;
	private $patterns;
	private $replacements;

	public function __construct(
		$lang = 'en'
	)
	{
		$this->lang = $lang;

		//Estabilish database connection
		$DB = new Database(
			DBHost,
			DBPort,
			DBName,
			DBUser,
			DBPassword,
			DBEngine
		);
		$this->arr = $DB->query(
			"
SELECT
	variable,
	result
FROM
	lang
WHERE
	country = :cc
	OR country = 'en'
ORDER BY
	country
			",
			array( "cc" => $this->lang ),
			PDO::FETCH_KEY_PAIR
		);

		// Allow global variables to be used
		$this->patterns = ['/{url}/'];
		$this->replacements = [URL];

		$this->arr = array_map(function ($value) {
			return preg_replace($this->patterns, $this->replacements, $value);
		}, $this->arr);

	}

	public function __get( $variable )
	{
		if ( !isset( $this->arr[$variable] ) )
			return "Error stamp: ".$_SERVER['REQUEST_URI']." ".$this->lang."_$variable";

		return $this->arr[$variable];
	}

	public function print( $variable ) // Deprecated, Will be removed soon
	{

		if ( !array_key_exists( $variable, $this->arr ) )
			return "Error stamp: ".$_SERVER['REQUEST_URI']." ".$this->lang."_$variable";


		return $this->arr[$variable];
	}
}
