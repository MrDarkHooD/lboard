<?php
namespace handler;
use PDO;
use \pdo\Database;
/*
	In this file database queries
	of column sid are made by
	$fingerprint because mobile
	and we cannot have nice things.
	Also I didn't yet update database
	to follow this.
*/

class User {

	public $sid;

	private $DB;
	private $DB2;
	private $access;
	private $expire_time;

	private $initialized = false;

	public $ip;
	public $id;
	public $uid;
	public $apikey;
	public $login_name;
	public $username;
	public bool $sidebar;
	public $theme;
	public $form;
	public $grid;
	public bool $followed_box;
	public bool $follow_created;
	public bool $follow_replied;
	public $functions;
	public $boardname;
	public $lang;
	public bool $registered;
	public $level;
	public $ban_ends;
	public $access_level;
	public $ban = false;
	public $ipbanned;
	public $css;
	public $last_post;
	public $last_thread;
	public $last_activity;

	public $countryCode;
	public $country;
	public $city;

	public bool $mobile;
	public bool $tor;
	
	private $fingerprint;

	public function __construct()
	{

		$this->DB = new Database(
			DBHost,
			DBPort,
			DBName,
			DBUser,
			DBPassword,
			DBEngine
		);

		$this->ip  = $_SERVER['REMOTE_ADDR'];
		$this->expire_time = 2147483647;

		$this->startSession();
		$this->sid = session_id();

		$this->fingerprint = sha1(
			$this->ip .
			$_SERVER['HTTP_USER_AGENT']
		);
	}

	private function startSession(): bool
	{
		if ( session_status() === PHP_SESSION_NONE )
		{
			session_start( [
				'cookie_path' 		=> '/',
				'cookie_lifetime' 	=> 0,
				'cookie_secure' 	=> true,
				'cookie_httponly' 	=> true,
				'cookie_domain' 	=> '.' . site->domain,
				'cookie_samesite' 	=> 'lax',
			] );

			if ( !session_id() )
			{
				trigger_error(
					"Error: startSession(): Session could not be started or resumed due to missing session_id().",
					E_USER_ERROR
				);
				exit;
				return false;
			}
			return true;
		}
		else
		{
			trigger_error(
				"Warning: startSession(): Cannot start session when session is already active",
				E_USER_NOTICE
			);
			return false;
		}
	}

	public function login(
		string $uid,
		string $apikey
	): string
	{
		if ( session_status() !== PHP_SESSION_NONE )
		{
			$this->logout();
		}

		$this->startSession();
		$this->sid = session_id();

		setcookie('apikey', $apikey, $this->expire_time, '/', '.' . site->domain);
		setcookie('uid', $uid, $this->expire_time, '/', '.' . site->domain);

		$this->initialized = false;

		$status = $this->insertLoginData( $uid );
		$this->initialize();

		#return $status;
		return $this->sid;
	}

	public function isLoggedIn( ): bool
	{
		if (
			!$this->sid ||
			!isset( $_COOKIE["uid"] )
		)
		{
			return false;
		}

		$dbCheck = $this->DB->single("
			SELECT 1
			FROM
				logins
			WHERE
				uid = :uid
				AND session_id = :sid
			",
			array(
				"uid" => $_COOKIE["uid"],
				"sid" => $this->fingerprint
			)
		);

		if( !$dbCheck )
		{
			return false;
		}

		return true;
	}

	public function initialize(): bool
	{

		#if( $this->initialized )
		#{
		#	echo "Error: Cannot initialize because initialized already done.";
		#	return false;
		#}

		if ( !$this->isLoggedIn() )
		{
			echo "Error: Cannot initialize because isLoggedIn() returned false";
			return false;
		}

		$userData = $this->DB->row( "
			SELECT
				id,
				uid,
				apikey,
				username,
				login_name,
				sidebar,
				style as theme,
				grid,
				followed_box,
				follow_created,
				follow_replied,
				default_functions as functions,
				lang,
				registered,
				ban_ends,
				bitwise_level as access_level,
				css,
				last_activity,
				(
					SELECT
						1
					FROM
						ban b
					WHERE
						b.ip = :ip
						AND b.endtime > CAST(EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) AS INT)
					LIMIT
						1
				) as ipbanned
			FROM
				users
			WHERE
				uid = :uid
			",
			array (
				"uid" => $_COOKIE["uid"],
				"ip" => $this->ip
			)
		);

		if ( !$userData )
		{
			echo "Error: Cannot initialize because userdata not found";
			return false;
		}

		foreach ( $userData as $key => $line )
		{
			$this->$key = $line;
		}

		if ( !$_SERVER['HTTP_USER_AGENT'] )
		{
			return false;
		}

		$this->mobile = (
			preg_match(
				'/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',
				$_SERVER['HTTP_USER_AGENT'] ) ||
			preg_match(
				'/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',
				substr( $_SERVER['HTTP_USER_AGENT'], 0, 4 )
			)
		);

		if ($this->ban_ends > time() or $this->ipbanned)
		{
			$user->ban = true;
		}

		$this->initialized = true;
		return true;

	}

	public function createUnregisteredUser(): bool
	{
		$random = rand(0,9999999);
		$apikey = md5(uniqid(rand(), true));
		$uid = uniqid();

		if ( $this->DB->query("
			SELECT 1
			FROM
				logins
			WHERE
				session_id = :sid
		",
			array(
				"sid" => $this->sid )
			)
		)
		{
			$this->logout();
			$this->initialized = false;
		}

		$status = $this->DB->insert(
			"users",
			array(
				"uid" => $uid,
				"password" => hash("sha256", $random),
				"apikey" => $apikey,
				"login_name" => $random,
				"last_activity" => time(),
				"registered" => 'false',
				"bitwise_level" => 1
			)
		);

		$this->login($uid, $apikey);

		if( $status )
		{
			return $this->insertLoginData( $uid );
		}

		return false;
	}

	public function logout(): bool
	{
		$status = $this->DB->query("
			DELETE FROM
				logins
			WHERE
				session_id = :sid
		",
			array( "sid" => $this->fingerprint )
		);

		session_destroy();

		return $status;
	}

	public function insertLoginData( $uid ): bool
	{
		$this->initializeLocation();

		$browser = get_browser(null, true);

		$status = $this->DB->insert(
			"logins",
			array(
				"uid" 			=> $uid,
				"session_id" 	=> $this->fingerprint,
				"start_time" 	=> time(),
				"end_time" 		=> $this->expire_time,
				"ip" 			=> $this->ip,
				"os" 			=> $browser["platform"],
				"browser" 		=> $browser["parent"],
				"location" 		=> "$this->city, $this->country"
			)
		);

		return (bool) $status;
	}

	public function initializeLocation(): void
	{
		libxml_use_internal_errors(true);

		// I spent approximately 3 hours and this was best i could figure out.
		// Overriding self defined error handler without $_GLOBALS, without
		// adding own variable to php.ini and without making pasta bolognese
		// to error handler it's surprisingly non-trival to bypass it clean.
		// Only reason for this is to prevent IP of user to be sent into telegram.
		restore_error_handler();
		$xml = simplexml_load_file( "http://www.geoyplugin.net/xml.gp?ip=" . $this->ip );
		restore_error_handler();

		if ($xml === false)
		{
			$xml = (object) [
				"geoplugin_countryCode" => "???",
				"geoplugin_countryName" => "???",
				"geoplugin_city" => "???"
			];
		}

		$this->countryCode = strtolower( $xml->geoplugin_countryCode );
		$this->country 	= $xml->geoplugin_countryName;
		$this->city 	= $xml->geoplugin_city;
	}

	public function initializeTor(): void
	{
		$reverseDns = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$this->tor = strpos($reverseDns, '.exit') !== false || strpos($reverseDns, '.tor') !== false;
	}
}
?>
