<?php
namespace handler;
use PDO;
use \pdo\Database;

class Log {
	public static function Insert($text)
	{
		$DB = new Database(
			DBHost,
			DBPort,
			DBName,
			DBUser,
			DBPassword,
			DBEngine
		);

		$DB->insert(
			"log",
			array (
				"userid" => user->id,
				"action" => $text,
				"time" => time(),
				"ip" => user->ip
			)
		);
	}
}
?>
