<?php
declare( strict_types=1 );
namespace handler;
use DateTime;
use ErrorException;
use Exception;
use \telegram\Telegram;

define('E_FATAL', E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING);
define('E_RECOVERABLE', E_WARNING | E_NOTICE | E_USER_WARNING | E_USER_NOTICE | E_STRICT);
class Error {

	private string	$http_host;
	private array 	$original_backtrace;
	private string	$full_uri;
	private bool $fatal = false;
	private array $recoverableErrors;
	private array $errorNames;

	private $tg;

	public function __construct()
	{

		$this->original_backtrace = (array) max( debug_backtrace() );

		$this->full_uri =
			$_SERVER['REQUEST_SCHEME'] . '://' .
			$_SERVER['SERVER_NAME'] . ":" .
			$_SERVER['SERVER_PORT'] .
			$_SERVER['REQUEST_URI'];

		$this->errorNames = [
			E_ERROR => "Fatal Error",
			E_USER_ERROR => "Fatal Error",
			E_WARNING => "Warning",
			E_USER_WARNING => "Warning",
			E_PARSE => "Parse Error",
			E_NOTICE => " Notice",
			E_USER_NOTICE => "Notice",
		];

		if(error_reporting() !== false)
		{
			$this->tg = new Telegram();
		}

		register_shutdown_function(array($this, 'shutdown'));
		set_exception_handler(array($this, 'ExceptionHandler'));
		set_error_handler(array($this, 'errorHandler'));
	}

	public function ExceptionHandler(
		$exception
	)
	{
		$this->fatal = true;

		if ( !(error_reporting() & $exception->getCode()) )
		{
			return;
		}

		$errorType = $this->errorNames[$exception->getCode()] ?? "Exception";

		$entry = $this->createLogEntry(
			$exception->getMessage(),
			$exception->getCode(),
			$exception->getFile(),
			$exception->getLine()
		);

		if( ini_get('log_errors') )
		{
			error_log(";start;" . $entry . ";end;", 0);
		}

		$this->tg->send($entry);
		$this->tg->halt();

		if( ini_get('display_errors') )
		{
			echo "<b>$errorType</b>: `" .
				htmlspecialchars($exception->getMessage()) . "` in " .
				htmlspecialchars($exception->getFile()) . ":" . $exception->getLine();
		}
	}

	public function errorHandler(
		int $errno,
		string $errstr,
		string $errfile = null,
		int $errline = null,
	): bool
	{
		if ( !(error_reporting() & $errno) )
		{
			return false;
		}

		if( !($errno & E_RECOVERABLE) )
		{
			throw new ErrorException($errstr, $errno, $errno, $errfile, $errline);
		}

		$entry = $this->createLogEntry(
			$errstr,
			$errno,
			$errfile,
			$errline
		);

		if ( ini_get('display_errors') )
		{
			$errorType = $this->errorNames[$errno] ?? "Exception";
			echo "<b>$errorType</b>: `" .
				htmlspecialchars($errstr) . "` in " .
				htmlspecialchars($errfile) . ":" . $errline;
		}

		$this->tg->send($entry);
		$this->tg->halt();

		return true;
	}

	public function shutdown (
		callable $callback = null
	): void
	{
		if($this->fatal)
		{
			echo "<br />
<pre>Admins have been notified.
Please wait couple minutes, there is 90% change that dev is
right now reading error message just like you are reading this
and in 5 seconds he will be fixing this error.
If you start pressing F5 or redoing what you just did 15 times
instantly, it will only annoy him and make his brain working harder
due constant message sounds from error notifications
(yes, errors are sent to him as messages on real time).
It is very likely that this error is fixed in next 10 minutes,
so don't forget us just yet, we are doing our best to keep users happy :)
</pre>";
		}

		echo PHP_EOL;
	}

	private function createLogEntry(
		$message,
		$code,
		$file,
		$line
	): string
	{
		$errorType = $this->errorNames[$code] ?? "Exception";

		$message = htmlspecialchars($message);
		$file = htmlspecialchars($file);
		$full_uri = htmlspecialchars($this->full_uri);
		$request =
			htmlspecialchars(
				$_SERVER['REQUEST_METHOD'] . " " .
				$_SERVER['SERVER_PROTOCOL'] . " " .
				$_SERVER['REQUEST_URI']
			);

		$datetime = new DateTime();
		$timestamp = $datetime->format(DateTime::ATOM);

		$entry = "\n<b>$errorType</b>: ";
		$entry .= "thrown in " . $file . ":" . $line . "\n";
		$entry .= "* Self: " . $full_uri . "\n";
		$entry .= "* Backtrace: " . $this->original_backtrace['file'] . ":" . $this->original_backtrace['line'] . "\n";
		$entry .= "* Client: " . $_SERVER['REMOTE_ADDR'] . "\n";
		$entry .= "* Domain: " . $_SERVER['SERVER_NAME'] . "\n";
		$entry .= "* Request: " . $request . "\n";
		$entry .= "* Timestamp: " . $timestamp . "\n";
		$entry .= "Message: " . $message . "\n";

		return $entry;
	}
}
?>
