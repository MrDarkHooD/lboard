<?php
date_default_timezone_set('Europe/Helsinki');

ini_set('session.cookie_lifetime', 15552000);
ini_set('session.gc_maxlifetime', 15552000);

$foribben      	= new stdClass();
$embedsettings 	= new stdClass();
$path		= new stdClass();
$settings 	= new stdClass();
$filters 	= new stdClass();
$upload 	= new stdClass();
$boards 	= new stdClass();

$settings->startyear	= 2013; // reminder

// deprecates soon
define('URL', "https://nyymi.net/");
define('MediaURL', "https://static.nyymi.net/");
define('MediaDir', $path->mediadir);
define('UploadURL', "https://i.nyymi.net/");
define('IconsDir', '/www/src/img/');

//Automatically change logo on certain time
$path->logo     = "logo.svg"; //default
if(date("j.n") == "30.4") $path->logo = "nazi.png";
if(date("j.n") == "20.4") $path->logo = "ganja.png";
if(date("j.n") == "17.5") $path->logo = "gay.png";
if(date("n") == "12")     $path->logo = "santa.png";
if(date("j.n") == "6.12") $path->logo = "finland.png";
$path->logo = MediaURL . "img/logo/" . $path->logo;

define('DBHost', '');
define('DBPort', );
define('DBName', '');
define('DBUser', '');
define('DBPassword', '');
define('DBEngine', '');

$settings->YouTubeAPIkey = "";
define('telegramAPIkey', '');
define('telegramChatId', '');

$styles = [
	"Terminal",
	"Yotsuba",
	"YotsubaB",
	"Northboard",
	"NewFag",
	"Tomorrow"
];

$languages = ["fi", "en"];

$filters->username = [
	"Anon",
	"Admin",
	"Adminstrator",
	"Sysop",
	"Mod",
	"Moderator",
	"Dev",
	"Developer",
	"Owner",
	"Superuser"
];

$filters->allowedSpecial = [
	"-",
	"_",
	" "
];

$banreasons = [
	"Laiton materiaali",
	"Sp&auml;mm&auml;ys",
	"Kaupallinen tai kilpaileva mainostaminen",
	"Haitalliset tiedostot tai linkit",
	"Laiton kaupankäynti",
	"Alaik&auml;ban",
	"Muu syy"
];

$upload->allowedExtensions = [
	"jpeg",
	"jpg",
	"jfif",
	"png",
	"gif",
	"webp",
	"mp3",
	"ogg",
	"ma4",
	"m4a",
	"webm",
	"mp4",
	"mov",
	"pdf",
	"zip",
	"rar",
	"7z",
	"swf",
	"aac"
];

$embedServices = [
	1 => [
		"name" => "Youtube",
		"domain" => "youtube.com",
		"video_baseurl" => "https://youtube.com/watch?v=",
		// https://stackoverflow.com/questions/2936467/parse-youtube-video-id-using-preg-match
		"regex_pattern" => '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i',
		"apiurl" => "https://www.googleapis.com/youtube/v3/videos?&id={embed}&key=$settings->YouTubeAPIkey&part=snippet,contentDetails,status",
		"player-icon" => "icon-youtube",
		"service-icon" => null,
		"disabled" => false,
		"data_extraction" => [
           	"embedtitle" => function($data) {
               	return $data['items'][0]['snippet']['title'];
            },
   	        "thumbimage" => function($data) {
       	        return $data['items'][0]['snippet']['thumbnails']['medium']['url'];
           	},
            "uploader" => function($data) {
   	            return $data['items'][0]['snippet']['channelTitle'];
       	    },
           	"duration" => function($data) {
				$duration = new DateInterval($data['items'][0]['contentDetails']['duration']);
               	return $duration->format('%H:%i:%s');
            },
           	"embeddable" => function($data) {
               	return $data['items'][0]['status']['embeddable'];
           	}
       	]
	],
	2 => [
		"name" => "Dailymotion",
		"domain" => "dailymotion.com",
		"video_baseurl" => "https://www.dailymotion.com/video/",
		"regex_pattern" => '%/video/([a-zA-Z0-9]*)?%m',
		"apiurl" => "http://www.dailymotion.com/services/oembed?url=https://www.dailymotion.com/video/{embed}",
		"player-icon" => null,
		"service-icon" => null,
		"disabled" => false,
		"data_extraction" => [
           	"embedtitle" => function($data) {
               	return $data['title'];
            },
   	        "thumbimage" => function($data) {
       	        return $data['thumbnail_url'];
           	},
            "uploader" => function($data) {
   	            return $data['author_name'];
       	    },
           	"duration" => function($data) {
				return 0;
            },
           	"embeddable" => function($data) {
               	return true;
           	}
       	]
	],
	3 => [
		"name" => "Vimeo",
		"domain" => "vimeo.com",
		"video_baseurl" => "https://vimeo.com/",
		//https://stackoverflow.com/questions/13286785/get-video-id-from-vimeo-url
		"regex_pattern" => '%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im',
		"apiurl" => "http://vimeo.com/api/v2/video/{embed}.json",
		"player-icon" => null,
		"service-icon" => "embed-vimeo",
		"disabled" => false,
		"data_extraction" => [
           	"embedtitle" => function($data) {
               	return $data[0]["title"];
            },
   	        "thumbimage" => function($data) {
       	        return $data[0]["thumbnail_large"];
           	},
            "uploader" => function($data) {
   	            return $data[0]["user_name"];
       	    },
           	"duration" => function($data) {
				return gmdate($dur, $data[0]["duration"]);
            },
           	"embeddable" => function($data) {
               	return true;
           	}
       	]
	],
	4 => [
		"name" => "BitChute",
		"domain" => "bitchute.com",
		"video_baseurl" => "https://www.bitchute.com/video/",
		"regex_pattern" => '%bitchute.com/video/([a-zA-Z0-9]*)/%',
		"apiurl" => "https://www.bitchute.com/oembed/?url=https://www.bitchute.com/video/{embed}/&format=json",
		"player-icon" => null,
		"service-icon" => "embed-bitchute",
		"disabled" => true,
		"data_extraction" => [
           	"embedtitle" => function($data) {
               	return $data["title"];
            },
   	        "thumbimage" => function($data) {
       	        return $data["thumbnail_url"];
           	},
            "uploader" => function($data) {
   	            return $data["author_name"];
       	    },
           	"duration" => function($data) {
				return 0;
            },
           	"embeddable" => function($data) {
               	return true;
           	}
       	]
	],
	5 => [
		"name" => "PornHub",
		"domain" => "pornhub.com",
		"video_baseurl" => "https://www.pornhub.com/view_video.php?viewkey=",
		"regex_pattern" => '%view_video.php\?viewkey=([a-zA-Z0-9]*)?%m',
		"apiurl" => "https://www.pornhub.com/webmasters/video_by_id?thumbsize=large_hd&id={embed}",
		"player-icon" => "icon-pornhub",
		"service-icon" => "embed-pornhub",
		"disabled" => false,
		"data_extraction" => [
           	"embedtitle" => function($data) {
               	return $data['video']['title'];
            },
   	        "thumbimage" => function($data) {
       	        return $data['video']['thumb'];
           	},
            "uploader" => function($data) {
   	            return null;
       	    },
           	"duration" => function($data) {
				return $data['video']['duration'];
            },
           	"embeddable" => function($data) {
               	return true;
           	}
       	]
	],
	6 => [
		"name" => "SoundCloud",
		"domain" => "soundcloud.com",
		"video_baseurl" => "https://soundcloud.com/",
		"regex_pattern" => '%soundcloud.com/([a-zA-Z0-9]*)%',
		"apiurl" => "https://soundcloud.com/oembed?format=json&url=https://soundcloud.com/{embed}",
		"player-icon" => null,
		"service-icon" => "embed-soundcloud",
		"disabled" => false,
		"data_extraction" => [
           	"embedtitle" => function($data) {
               	return $data['title'];
            },
   	        "thumbimage" => function($data) {
       	        return $data['thumbnail_url'];
           	},
            "uploader" => function($data) {
   	            return $data['author_name'];
       	    },
           	"duration" => function($data) {
				return ;
            },
           	"embeddable" => function($data) {
               	return ;
           	}
       	]
	],
	7 => [
		"name" => "Strawpoll.me",
		"domain" => "strawpoll.me",
		"video_baseurl" => "https://www.starwpoll.me",
		"regex_pattern" => '%strawpoll.me/([0-9]*)%',
		"apiurl" => "http://www.strawpoll.me/{embed}",
		"player-icon" => "icon-pie-chart",
		"service-icon" => null,
		"disabled" => true,
		"data_extraction" => [
           	"embedtitle" => function($data) {
               	return ;
            },
   	        "thumbimage" => function($data) {
       	        return ;
           	},
            "uploader" => function($data) {
   	            return $data['author_name'];
       	    },
           	"duration" => function($data) {
				return ;
            },
           	"embeddable" => function($data) {
               	return ;
           	}
       	]
	]
];

define("embedServices", $embedServices);

$foribben->image = ['<','>', '..'];

$boards->name = array();
$boards->redirect = ["h" => "uu"];

$bbfrom = [
	'/\[b\](.*?)\[\/b\]/is',
	'/\[i\](.*?)\[\/i\]/is',
	'/\[s\](.*?)\[\/s\]/is',
	'/\[u\](.*?)\[\/u\]/is',
	'/\[big\](.*?)\[\/big\]/is',
	'/\[small\](.*?)\[\/small\]/is',
	'/\[spoiler\](.*?)\[\/spoiler\]/is',
	'/\[code\](.*?)\[\/code\]/is',
	'/\[quote\](.*?)\[\/quote\]/is',
	'/\[o\](.*?)\[\/o\]/is',
	'/\[yellow\](.*?)\[\/yellow\]/is',
	'/\[blue\](.*?)\[\/blue\]/is',
	'/\[green\](.*?)\[\/green\]/is',
	'/\[red\](.*?)\[\/red\]/is',
	'/\[pink\](.*?)\[\/pink\]/is',
	'/\[brown\](.*?)\[\/brown\]/is',
	'/\[black\](.*?)\[\/black\]/is',
	'/\[white\](.*?)\[\/white\]/is',
	'/\[orange\](.*?)\[\/orange\]/is',
	'/\[purple\](.*?)\[\/purple\]/is',
	'/\[gray\](.*?)\[\/gray\]/is',
	'/\[shadow\](.*?)\[\/shadow\]/is',
	'/\[fortune\](.*?)\[\/fortune\]/is'
];
$bbto = [
	'<b>$1</b>',
	'<i>$1</i>',
	'<s>$1</s>',
	'<u>$1</u>',
	'<big>$1</big>',
	'<small>$1</small>',
	'<span class="spoiler">$1</span>',
	'<pre class="code"><code>$1</code></pre>',
	'<pre class="bb_quote">$1</quote>',
	'<span class="overline">$1</span>',
	'<span class="yellow">$1</span>',
	'<span class="blue">$1</span>',
	'<span class="green">$1</span>',
	'<span class="red">$1</span>',
	'<span class="pink">$1</span>',
	'<span class="brown">$1</span>',
	'<span class="black">$1</span>',
	'<span class="white">$1</span>',
	'<span class="orange">$1</span>',
	'<span class="purple">$1</span>',
	'<span class="gray">$1</span>',
	'<span class="shadow">$1</span>',
	'<span class="fortune">$1</span>'
];

define('bbfrom', $bbfrom);
define('bbto', $bbto);
?>
