<?php
$sql = new stdClass();

$sql->messageCount = "
SELECT
	MAX(id) AS id
FROM
	board_messages
";

$sql->usersOnline = "
SELECT
	COUNT(id)
FROM
	users
WHERE
	last_activity > EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - INTERVAL '5 minutes'))
";

$sql->userCountPast24h = "
SELECT
	COUNT(id)
FROM
	users
WHERE
	last_activity > EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - INTERVAL '24 hours'))
";

$sql->frontPageRecentImages = "
SELECT
	file.filename,
	file.filedir,
	file.extension,
	board_messages.area,
	board_messages.id as messageid,
	area.dir as dir,
	area.nsfw as nsfw
FROM
	file
INNER JOIN
	board_messages ON board_messages.filedir = file.filedir
INNER JOIN
	area ON board_messages.area = area.id
WHERE
	(
		file.imagetype = 'image'
		OR file.imagetype = 'video'
	)
	AND area.hidden = false
	AND area.bitwise_level = 1
	AND board_messages.deleted = false
ORDER BY
	file.id DESC
LIMIT
	20
";

$sql->frontPageRecentThreads = "
SELECT board_messages.id,
	board_messages.topic,
	area.name as subboard
FROM
	board_messages
LEFT JOIN
	area ON board_messages.area = area.id
WHERE
	board_messages.deleted = 'false'
	AND board_messages.startpost = 'true'
	AND area.bitwise_level = 1
ORDER BY
	board_messages.id DESC
LIMIT
	50
";

$sql->frontPageRules = "
SELECT
	p.id AS parent_id,
	p.reason AS parent_reason,
	c.reason AS child_reason
FROM
	banreasons p
LEFT JOIN
	banreasons
	c ON p.id = c.parent
WHERE
	p.parent = 0
";

$sql->openPlayerDataQuery = "
SELECT
	CASE
		WHEN
			bm.embed_id IS NULL
		THEN
			f.filedir
		ELSE
			embed.filedir
	END AS filedir,
	COALESCE(f.filename, NULL) AS basename,
	COALESCE(f.extension, NULL) AS extension,
	CONCAT_WS('.', f.filename, f.extension) as filename,
	COALESCE(f.imagetype, 'embed') AS filetype,
	COALESCE(f.mimetype, NULL) AS mimetype,
	COALESCE(f.lenght, 0) AS duartion,
	COALESCE(embed.type, NULL) AS embedservice,
	COALESCE(embed.title, NULL) AS embedtitle,
	COALESCE(embed.duration, NULL) AS embedduration,
	COALESCE(embed.code, NULL) AS embedcode
FROM
	board_messages bm
LEFT JOIN
	file f ON bm.filedir = f.filedir
LEFT JOIN
	embed ON bm.embed_id = embed.id
WHERE
	bm.id = :id
    AND bm.file_deleted = 'false'
    AND bm.deleted = 'false'
";
?>

