<?php
// required headers
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Origin: https://nyymi.net");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
header("Content-Type: application/json; charset=UTF-8");

// Normal, mandatory header request query from browser.
// Comes before actual request every time, no need to go further.
// It does not contain any needed data so it would trigger error very soon
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS')
{
	exit;
}

//Load requirments
require_once('/www/src/settings.php');
require_once('/www/src/loader.php');

$errorHandler = new ErrorHandler();

function errorAndStop($code, $publicText, $type)
{

	$httpResponseCodes = [
		401 => "Unauthorized",
		404 => "Not Found",
		410 => "Gone",
		415 => "Unsupported Media Type",
		422 => "Unprocessable Content",
		423 => "Locked",
		429 => "Too Many Requests",
		503 => "Service Unavailable"
	];
	
	http_response_code($code);
	$array = [];
	$array["error"] = [
		"code" => $code,
		"message" => $httpResponseCodes[$code]
	];
	
	$array["success"] = 0;
	$array["errorTitle"] = $httpResponseCodes[$code];
	$array["errorDesc"] = $publicText;
	$array["errorType"] = $type;
	
	die(json_encode($array));
}

$DB = new Database(
	DBHost,
	DBPort,
	DBName,
	DBUser,
	DBPassword,
	DBEngine
);

$site = (object) $DB->row("SELECT * FROM site_settings", []);

$user = new User();

if ( $_SERVER['REQUEST_URI'] != "/login.php" )
{
	if ( !$user->initialize() )
	{
		die(json_encode(["no"]));
	}
}

//Set REQUEST variables, deprecates soon
if(isset($_REQUEST['query'])) $query = $_REQUEST['query'];
if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];

//Parse variables, deprecates soon
if(isset($id)) $id = preg_replace('/\D/', '', $id); //Allow only numbers

$lang	= new Language('en');
$error	= new Error();
$access = new Access();
$access->initialize();
