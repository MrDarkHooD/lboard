<?php
$free_access = true;
require_once("inc/top.php");

$countryCode = $_GET['countryCode'] ?? 'en';

$result = $DB->query("
	SELECT variable, result
	FROM
		lang
	WHERE
		lang = ?
	GROUP BY
		variable
	",
	array( $countryCode )
);

if( !$result )
{
	errorAndStop(404, "Language code not found", "error");

$output = [];
foreach($result as $lang)
{
	$output[$lang['variable']] = $lang['result'];
}

echo json_encode($output);
