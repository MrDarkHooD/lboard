<?php
require_once("inc/top.php");

if ( !$user->logout() )
{
	pushNotification(
		500,
		"Failed to logout",
		"error"
	);
	exit;
}

pushNotification(
	200,
	"Logged out succesfully",
	"success"
);
?>
