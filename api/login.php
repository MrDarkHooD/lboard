<?php
include("inc/top.php");
$posthandler = new handler\Post();

$post_fields = [
	["username", true, TYPE_STR, CHARSET_ALPHANUM, $filters->allowedSpecial],
	["password", true, TYPE_PASSWORD]
];

if ( !$post = $posthandler->ValidatePOSTFields( $post_fields ) )
{
	pushNotification(
		422,
		lang->InvalidPostData,
		"error"
	);
}

$line = $DB->row(
	"SELECT uid, password, apikey FROM users WHERE login_name = ?",
	array($post->username));

if ( !$line )
{
	pushNotification(
		401,
		lang->LoginCredentialsAreWrong,
		"error"
	);
}

if ( !password_verify( $post->password, $line['password'] ) )
{
	pushNotification(
		401,
		lang->LoginCredentialsAreWrong,
		"error"
	);
}

$newSid = $user->login(
	$line['uid'],
	$line['apikey']
);

pushNotification(
	200,
	lang->LoginSuccessful,
	"success"
);
?>
