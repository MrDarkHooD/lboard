<?php
	require_once('inc/top.php');

	// Partial message data for hovered quoted message
	if($query == "quoted")	{

		$result = $DB->row("
			SELECT
				id,
				topic,
				message,
				username,
				to_char(to_timestamp(unixtime)::timestamp, 'YYYY-MM-DD\"T\"HH24:MI:SS\"Z\"') as time,
				filedir
			FROM 
				board_messages
			WHERE
				deleted = 'false'
				AND id = ?;",
			array( $id )
		);

		if ( $result )
		{
			$result['username'] = ($result['username']) ?? DefaultPostname;
		}
		else
		{
			errorAndStop("404", "Message not found", "warning");
		}
	}


	require_once('inc/bottom.php');
?>
