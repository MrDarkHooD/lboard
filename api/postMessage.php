<?php
require_once("inc/top.php");

$textHandler = new handler\TextHandler();

//if (!$settings->postavaible && !RightsAdmin)
//	errorStop($lang->print('ErrorPostingNotAvaibled'));

if (user->ban)
{
	pushNotification(
		401,
		lang->ErrorYouCannotSendMsgBanned,
		"error"
	);
}
$insert = new stdClass();

$user->initializeLocation();
$user->initializeTor();

$posthandler = new handler\Post();
$post_fields = [ //[(string)key, (bool)must_isset, (int)type]
	["topic", false, TYPE_STR],
	["message_box", true, TYPE_STR],
	["embed", false, TYPE_STR],
	["actions", false, TYPE_STR],
	["startpost", true, TYPE_BOOL],
	["thread_id", false, TYPE_INT],
	["areadir", true, TYPE_STR],
	["filedir", false, TYPE_STR, CHARSET_ALPHANUM],
	["captcha", $site->require_captcha, TYPE_STR]
];

if ( !$post = $posthandler->ValidatePOSTFields( $post_fields ) )
{
	pushNotification(
		422,
		lang->InvalidPostData,
		"error"
	);
}

$post->threadid = $post->thread_id ?? 0;

if(!isset($post->topic))
{
	$post->topic = "";
}

/* file checks */ 
if ($post->filedir)
{
	// make this better, use db
	if(!is_dir("/www/upload/" . $post->filedir))
	{ 
		pushNotification(
			404,
			lang->ErrorInvalidFilePostMessage,
			"error"
		);
	}
	else
	{
		$post->embedcode = "";
	}
}

/* Embed code */ 
// if embedcode then filedir empty
if(
	!empty($post->embed) &&
	empty($post->filedir)
)
{
	require_once("uploadEmbed.php");
}

//Area check
$query = $DB->row(
	"SELECT id, bitwise_level FROM area WHERE dir = ?",
	array( $post->areadir )
);

if ( !$query  ) 
{
	pushNotification(
		404,
		lang->ErrorAreaDoesNotExist,
		"error"
	);
}

$areaid = $query["id"];
$arealevel = $query["bitwise_level"];


if ( !$access->hasLevel( user->access_level, $arealevel ) )
{
	pushNotification(
		401,
		lang->ErrorNoRights,
		"error"
	);
}

if (
	time() - user->last_post < site->message_cooldown_time &&
	!$access->hasLevel( user->access_level, LEVEL_MODERATOR )
)
{
	pushNotification(
		429,
		lang->ErrorSendingMessagesTooFast,
		"error"
	);
}

//CAPTCHA this is broken and software doesn't have captcha addon
#if (!RightsMod && $settings->captcha) { // && $_COOKIE["ct"] < $settings->captchatime
#    $post->captcha = $_POST["captcha"];
#    if ($post->captcha != UserData["last_captcha"])
#        pushNotification(lang->ErrorCaptchaIncorrect);
#}

#if ($textHandler->ContainsBlacklistedStrings($post->message))
#	pushNotification(lang->ErrorMessageContainedBlacklistedWord);

/*************/
/* Functions */
/*************/

# Some of these need to be enabled with empty message, 
# purely for moderation purposes. These all can be first
# thing to process, it doesn't take so much resources.
$available_functions = [ // [name, level]
	["noname", LEVEL_ALL],
	["hideop", LEVEL_ALL],
	["filefloatright", LEVEL_ALL],
	["sage", LEVEL_ALL],
	["noko", LEVEL_ALL],
//	["removeexif", LEVEL_ALL], moved to fileupload (later ":D")
	["fortune", LEVEL_ALL],
	["forcecountry", LEVEL_ALL],
	["modtag", LEVEL_MODERATOR],
	["admintag", LEVEL_ADMIN],
	["lock", LEVEL_MODERATOR],
	["delock", LEVEL_MODERATOR],
	["sticky", LEVEL_MODERATOR],
	["desticky", LEVEL_MODERATOR]
];

$post->actions = explode( " ", $post->actions );

$action	= new stdClass();

foreach ( $available_functions as list( $command, $level ) )
{
	if (
		in_array( $command, $post->actions ) &&
		$access->hasLevel( $user->access_level, $level )
	)
	{
		$action->{$command} = true;
	}
	else
	{
		$action->{$command} = false;
	}
}

if ( $action->fortune )
{
	$fortune = json_decode(file_get_contents("https://api.justyy.workers.dev/api/fortune"));
	$post->message_box .= "\n\n[fortune]{$fortune}[/fortune]";
}

$insert->username = ($action->noname) ? null : (user->username ?? site->default_postname);

$post->CountryCode = ($action->forcecountry) ? user->countryCode : null;

$post->message = $textHandler->ParseMessage( $post->message_box, site->max_message_characters );


if ($post->startpost) {
	
	if ( $textHandler->IsMessageEmpty( $post->message ) )
	{
		pushNotification(
			422,
			lang->ErrorStartpostMustContainText,
			"warning"
		);
	}
	
	if (
		empty( $post->filedir ) &&
		empty( $insert->embedcode )
	)
	{
		pushNotification(
			422,
			lang->ErrorStartpostRequiresFileOrEmbed,
			"warning"
		);
	}
    if (!$post->topic)
		$post->topic = $post->message;

	if (
		time() - $user->last_thread < $site->thread_cooldown_time &&
		!$access->hasLevel( $user->access_level, LEVEL_DEVELOPER )
	)
	{
		pushNotification(
			429,
			lang->ErrorCreatingThreadsTooFast,
			"error"
		);
	}
	
	$action->noko = 1;
	$post->topic = $textHandler->ParseTopic( $post->topic, $site->max_topic_characters );
	$post->threadid = null;
}
else
{
	$post->startpost = false;
	
	$startpostData = $DB->row(
		"SELECT deleted, locked FROM board_messages WHERE id = ?",
		array( $post->threadid )
	);

	if ( !$startpostData )
	{
		pushNotification(
			404,
			lang->ErrorThreadDoesntExist,
			"warning"
		);
	}
	
	if ( $startpostData["deleted"] )
	{
		pushNotification(
			410,
			lang->ErrorThreadDeleted,
			"error"
		);
	}
	
	if (
		!$access->hasLevel( user->access_level, LEVEL_MODERATOR ) &&
		$startpostData["locked"]
	)
	{ 
		pushNotification(
			423,
			lang->ErrorThreadLocked,
			"warning"
		);
	}
}

if (
	empty( $post->filedir ) &&
	empty( $insert->embedcode ) &&
	$textHandler->IsMessageEmpty( $post->message )
)
{
	pushNotification(
		422,
		lang->ErrorPostMustContainSomething,
		"warning"
	);
}

if ( substr_count( $post->message, "\n" ) > site->max_linebreaks )
{
	pushNotification(
		422,
		lang->ErrorTooManyLinebreaks,
		"warning"
	);
}

#if($debugSettings->postingDisabled)
#{
#	pushNotification(422, $lang->print("UnderMaintenance"), "error");
#}

// unique user id for this thread
$insert->tuid = md5( md5( user->uid ) . $post->threadid );

$DB->beginTransaction();

$insert->lastInsertId = $DB->insert(
	"board_messages",
	array
	(
		"area" 			=> $areaid,
		"uid" 			=> user->uid,
		"ip" 			=> user->ip,
		"message" 		=> $post->message,
		"topic" 		=> $post->topic,
		"filedir" 		=> $post->filedir,
		"startpost" 	=> $post->startpost,
		"embed_id" 		=> $insert->embedid ?? null,
		"username" 		=> $insert->username,
		"moderator" 	=> $action->modtag,
		"admin" 		=> $action->admintag,
		"floatright" 	=> $action->filefloatright,
		"sage" 			=> $action->sage,
		"country" 		=> $post->CountryCode,
		"mobile" 		=> user->mobile,
		"tor" 			=> user->tor,
		"hideop" 		=> $action->hideop,
		"unixtime" 		=> time(),
		"threadid" 		=> $post->threadid,
		"locked" 		=> $action->lock,
		"sticky" 		=> $action->sticky,
		"tuid" 			=> $insert->tuid
	)
);

if ($post->startpost)
{
	$post->threadid = $insert->lastInsertId;
}

if (
	!$action->sage or
	$post->startpost
)
{
	$DB->query("UPDATE board_messages SET bump = ? WHERE id = ?",
	array($insert->lastInsertId, $post->threadid));
}

if ( $action->lock )
{
	$DB->query("UPDATE board_messages SET locked = ? WHERE id = ?",
    	    array( $action->lock, $post->threadid));
	#Log::Insert("Locked thread " . $insert->lastInsertId);
}

if ( $action->sticky )
{
	$DB->query("UPDATE board_messages SET sticky = ? WHERE id = ?",
    	    array($action->sticky, $post->threadid));
	#Log::Insert("Stickyed thread " . $insert->lastInsertId);
}


//Saving replies to another table, this can be done better
$fl_array = preg_match_all("/(>>[0-9]*)/", $post->message_box, $array);
for (
	$i = 0;
	$i != $fl_array;
	$i++
)
{
    $input = preg_replace("/>/", "", $array[0][$i]);
	# These column names are bad, replies has only 1 value so it's not *s
	# Also "messageid" should be parentid or something more distinct
    $DB->query("INSERT INTO replies (messageid, replies) VALUES (:messageid, :replies)",
		array(
			"messageid" => $insert->lastInsertId,
			"replies" => $input
		)
	);
}

if ( $post->startpost && user->follow_created )
{
    $DB->insert(
		"followed",
    	array(
			"threadid" => $insert->threadid,
			"userid" => $user->id,
			"lastid" => $insert->threadid
		)
	);

}

if ( !$post->startpost && user->follow_replied )
{
    $isFollowed = $DB->single(
		"SELECT 1 FROM followed WHERE threadid = :threadid AND userid = :uid;",
        array(
			"threadid" => $insert->threadid,
			"uid" => $user->id
		)
	);
	
    if ( !$isFollowed )
		$DB->insert(
			"followed",
			array(
				"threadid" => $insert->threadid,
				"userid" => user->uid,
				"threadid" => $insert->threadid
			)
		);
}

if ($post->startpost)
{
	$DB->query(
		"UPDATE users SET last_thread = ? WHERE uid = ?",
		array(
			time(),
			user->uid
		)
	);
}

$DB->query(
	"UPDATE users SET last_post = ? WHERE uid = ?",
	array(
		time(),
		user->uid
	)
);

$DB->commit();

$output = [
	"success" => true,
	"postid" => $insert->lastInsertId
];

if( $insert->lastInsertId == $post->threadid )
{
	$output["forward"] = "/" . $post->areadir . "/" . $post->threadid;
}
elseif( !$action->noko )
{
	$output["forward"] = "/" . $post->areadir . "/";
}

echo json_encode($output);
?>

