<?php
require_once('inc/top.php');

switch ( $query )
{
	case "newestid":

		$line = $DB->single("SELECT MAX(id) AS id FROM board_messages;", array());
		$result = ["newestid" => $line];
		break;
		
	
	case "truecount": //Count of messages in database
		
		$line = $DB->single("SELECT COUNT(id) FROM board_messages;", array());
		$result = ["truecount" => $line];
		break;
	
	case "followed": //Gets users followed list

		break;

	case "banreasons":

		$result = $DB->query("SELECT reason FROM banreasons", array());
		break;

	case "filerules":

		$result = [
			"max_filesize"			=> $site->max_filesize * 1024 * 1024,
			"max_filename_length"	=> $site->max_file_name_length,
			"allowed_extensions"	=> $upload->allowedExtensions,
			"allowed_mimetypes"		=> $upload->filetypes,
			"extension_to_filetype"	=> $upload->type,
			"forbitten_characters"	=> $foribben->image,
		];
		break;

	case "accountrules":

		$result = [
			"max_characters_in_username"	=> $site->max_username_characters,
		];
		break;

	case "messagerules":

		$result = [
			"max_characters_in_message"			=> $site->max_message_characters,
			"max_characters_in_topic"			=> $site->max_topic_characters,
			"max_newlines_in_message"			=> $site->max_linebreaks,
		];
		break;

	default:
		
		pushNotification(
			422,
			"Query not valid",
			"No query or invalid query set in _GET."
		);
		break;
}

require_once('inc/bottom.php');
?>
