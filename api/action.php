<?php
require_once('inc/top.php');

switch ( $_GET["action"] )
{

	case "deleteMessage":
		$id = $_POST["id"];
		if ( !is_numeric($id) )
		{
			pushNotification(
				422,
				"ID is not numeric",
				"error"
			);
			exit;
		}
		
		$fileonly = false;
		# ugly stupid code, but it works for now, later gonna enable this to all
		if( !$access->hasLevel( $user->level, LEVEL_MODERATOR ) )
		{
			$fileonly = false;
		}

		$muid = $DB->single(
			"SELECT uid FROM board_messages WHERE id = ? AND deleted = 'false'",
			array( $id )
		); 
		
		if ( !$muid )
		{
			pushNotification(
				404,
				"Message not found",
				"error"
			);
			exit;
		}
		
		if(
			$muid != $user->uid &&
			!$access->hasLevel( UserAccessLevel, LEVEL_MODERATOR )
		)
		{
			pushNotification(
				401,
				"Permission denied",
				"error"
			);
			exit;
		}
		

		if($fileonly)
		{
			$DB->query(
				"UPDATE board_messages SET file_deleted = 'true' WHERE id = ?",
				array($id)
			);

			pushNotification(
				200,
				"File from message $id deleted succesfully.",
				"success"
			);
			
		}
		else
		{
			$sql_array = array(
				"id" => $id
			);
			
			$DB->query("UPDATE board_messages SET deleted = 'true' WHERE id = :id", $sql_array);
			$DB->query("DELETE from replies WHERE replies = :id OR messageid = :id", $sql_array);
			$DB->query("DELETE from followed WHERE threadid = :id", $sql_array);
			$DB->query("DELETE from hidden WHERE messageid = :id", $sql_array);

			pushNotification(
				200,
				"Message $id deleted succesfully.",
				"success"
			);
		}
		
		if ( $muid != $user->uid )
		{
			ActionLog::Insert("Removed message {$id}.");
		}

		break;
		
	case "hideMessage":
		
		$DB->insert(
			"hidden",
			array(
				"messageid" => $id,
				"userid" => $user->uid
			)
		);
		break;
		
	case "report":
		if( !isset($_GET['check']) )
		{
			$result = $message->Report($id);
		}
		else {
			$result = $message->Unreport($id);
		}
		break;
		
	case "lock":

		
		break;
		
	case "sticky":

		
		break;
		
	case "userLevels":
		
		Log::Insert("Updated user levels.");
		break;
		
	case "toggleGrid":
		
		$DB->query(
			"UPDATE users SET grid = not grid WHERE uid = :uid",
			array($user->uid)
		);
		break;
		
	default:

		pushNotification(
			422,
			"Action not valid",
			"No action or invalid action set in _GET."
		);
		break;

}
?>
