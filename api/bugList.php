<?php
$free_access = true;
require_once('inc/top.php');

$sql = "
	SELECT 
		bug,
		mobile,
		fixed,
		unix_timestamp as timestamp
	FROM
		known_bugs
";

$opts = [["f","fixed",'true'], ["nf","fixed",'false'], ["m","mobile",'true'], ["nm","mobile",'false']];

if(isset($_GET["m"]) or isset($_GET["f"]) or isset($_GET["nm"]) or isset($_GET["nf"]))
{
	if( (isset($_GET["m"]) && isset($_GET["nm"])) or (isset($_GET["f"]) && isset($_GET["nf"])) )
		die(json_encode(["ei"]));
	
	$sql .= "WHERE ";

	$arr = [];

	foreach ($opts as $opt)
		if(isset($_GET[$opt[0]]))
			array_push($arr, $opt[1] . " = " . $opt[2]);

	$sql .= implode(" AND ", $arr);
}

$sql .= " ORDER BY mobile, fixed, id;";

$bugs = $DB->query($sql, array() );

print(json_encode($bugs));
