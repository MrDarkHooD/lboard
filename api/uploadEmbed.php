<?php

if(!URLexist($post->embed))
{
	pushNotification(
		404,
		lang->ErrorProvitedEmbedUrlReturned404,
		"error"
	);
	exit;
}

//autodetect
foreach( $embedServices as $key => $value )
{
	if ( strstr( $post->embed, $value["domain"] ) )
	{
		$insert->embedtype = $key;
		$embeddata = $embedServices[$key];
		break;
	}
}

$insert->filedir = uniqid();
$post->filedir = $insert->filedir;
if(!isset($embeddata))
{
	pushNotification(
		503,
		lang->ErrorEmbedServiceNotSupported,
		"error"
	);
	exit;
}

if ( $embeddata["disabled"] )
{
	pushNotification(
		503,
		lang->ErrorEmbedServiceDisabled,
		"error"
	);
	exit;
}

if( !preg_match( $embeddata["regex_pattern"], $post->embed, $match ) )
{
	pushNotification(
		404,
		lang->ErrorEmbedCodeNotFound,
		"error"
	);
	exit;
}

//Embed code is okay and service is known
$insert->embedcode = ($insert->embedtype == 3) ? $match[3] : $match[1];
$apiurl = preg_replace('/{embed}/', $insert->embedcode, $embeddata["apiurl"]);
$headers = file_get_contents($apiurl);
$data = json_decode($headers, true);

foreach ( $embeddata["data_extraction"] as $field => $extraction ) {
    $insert->$field = $extraction($data);
}

$insert->embedcode = htmlspecialchars( $insert->embedcode );				
mkdir(site->upload_dir.$insert->filedir);
mkdir(site->upload_dir.$insert->filedir."/t/");

if(!copy($insert->thumbimage, site->upload_dir.$insert->filedir.'/t/thumb.avif'))
{
	pushNotification(
		402,
		lang->ErrorEmbedThumbnailFailed,
		"error"
	);
	exit;
}

$cmdoutput = escapeshellarg(site->upload_dir.$insert->filedir.'/t/thumb.avif');
exec("nice --adjustment=19 avifenc '$cmdoutput' -o '$cmdoutput'");

if (
	!$insert->embedid = $DB->single(
		"SELECT id FROM embed WHERE code = ?",
		[$insert->embedcode]
	)
)
{
	$insert->embedid = $DB->insert(
		"embed",
		array(
			"code" => $insert->embedcode,
			"type" => $insert->embedtype,
			"title" => $insert->embedtitle,
			"duration" => $insert->duration,
			"uploader" => $insert->uploader,
			"filedir" => $insert->filedir
		)
	);
}
?>

